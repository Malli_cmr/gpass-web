var userdata ;
var gpassregister = {
  
   registeruser : function (registerdata){
     
     $.ajax({
      type: "POST",
      url: hostName+"/api/register",
      async: false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         userdata = succdata.data; 
      }}
      });    
    return userdata;
     
   },
   
   updateUser : function (data, id){
     
     $.ajax({
      type: "PUT",
      url: hostName+"/api/updateuser/"+id,
      async: false,
      data : data,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         userdata = succdata.data;                 
      }}
      });    
    return userdata;
     
   },
   
   getuserbyemail : function(emailid){
      var userdata;
     
     $.ajax({
      type: "GET",
      url:hostName+"/api/userinfobyemail/"+emailid,
      async: false,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
          userdata = succdata.data;
         
         
         }}});
         
         return userdata;
         
         },
  
  getOfficers : function(checked, prefix){
      var userdata;
     
     if(checked) {
	     $.ajax({
	      type: "GET",
	      url:hostName+"/api/officerinfo",
	      async: false,
	      dataType: "json",
	      headers:{
	                "Content-Type":"application/json",
	                 "Access-Control-Allow-Origin": "*",
	                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
	                "Authorization": bauth,
	            },
	      success: function (data){
	        if(data != null){
	         var appendTo = document.getElementById(prefix+"officerslist");
	         
	         var label = document.createElement("label");
	         label.setAttribute("for", prefix+"gp_usr_emp_proxy_for");
	         label.innerHTML = 'Proxy for';
	         
	         var select = document.createElement("select");
	         select.setAttribute("name", prefix+"gp_usr_emp_proxy_for");
	         select.setAttribute("class", "form-control input-lg");
	         select.setAttribute("required", "");
	         select.setAttribute("id", prefix+"gp_usr_emp_proxy_for");
	         var result = data.data;	
	         var option;  
	         
	         option = document.createElement("option");
         	 option.value = '';
         	 option.text = 'Select';
         	 select.appendChild(option);
	         	       
	         for(var i=0; i < result.length; i++) {
	         	option = document.createElement("option");
	         	option.value = result[i].gp_usr_id;
	         	option.text = result[i].gp_usr_emp_name;
	         	select.appendChild(option);
	         }
	         appendTo.appendChild(label);
	         appendTo.appendChild(select);
	         }}});
         }
        else {
        	var appendTo = document.getElementById(prefix+"officerslist");
        	appendTo.innerHTML = '';
        }
       
         },
         
  getuserbymobile : function(mobileno){
    
    $.ajax({
      type: "GET",
      url:hostName+"/api/userinfobymbl/"+mobileno,
      async: false,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
          userdata = succdata.data;
  }}});
  return userdata;
  
  },
  
  getuserbyid : function(id){
    
    $.ajax({
      type: "GET",
      url:hostName+"/api/userinfo/"+id,
      async: false,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
          userdata = succdata.data;
  }}});
  return userdata;
  
  },
  
  getAllUsers : function(){
    
    $.ajax({
      type: "GET",
      url:hostName+"/api/allusers",
      async: false,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
          userdata = succdata.data;
  }}});
  return userdata;
  
  }
 
};

var retnot;
var gpassnotification ={
  
  sms : function (mobileno, text) {
    
     var params;
     var registerdata;
     
    
   params = {
                 
  "mobileno":"91"+mobileno,
  "text":text
                  
                };

  registerdata = JSON.stringify(params);
    
     $.ajax({
      
      type: "POST",
      url:hostName+"/api/apmnt/smsnotification",
      async: true,
      global:false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      
      success: function (data){
        if(data != null){
         var succdata = data;
         var alertdata1 = succdata.data;
         var msg = "Your Gate Pass request submitted successfully";
        
        }
      },
      });
    
  },
  email : function (emailid, subject, body) {
    
     var params;
     var registerdata;
     
    
   params = {
                 
  "email":emailid,
  "subject":subject,
  "body":body
                  
                };

  registerdata = JSON.stringify(params);
    
     $.ajax({
      type: "POST",
      url:hostName+"/api/apmnt/mailnotification",
      async: true,
      global:false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var alertdata2 = succdata.data;
         var msg = "Your Gate Pass request submitted successfully";                  
        }
      },
      });
    
  },
  
};

$(document).ready(function() {
    var currentDate = new Date();
    var twoDigitMonth=((currentDate.getMonth()+1)>=10)? (currentDate.getMonth()+1) : '0' + (currentDate.getMonth()+1);  
    var twoDigitDate=((currentDate.getDate())>=10)? (currentDate.getDate()) : '0' + (currentDate.getDate());
    var createdDateTo = twoDigitDate + "-" + twoDigitMonth + "-" + currentDate.getFullYear();
    
    if($("#search_fromdate").length > 0)
        $("#search_fromdate").val(createdDateTo);
    
    if($("#search_todate").length > 0)
        $("#search_todate").val(createdDateTo);
    
    $("input[type=text].numeric").on("keypress", function(event) {
        evt = (event) ? event : window.event;			
        var charCode = (evt.which) ? evt.which : evt.keyCode;

        var maxlength = typeof($(this).attr('max-length')) != "undfined" ? $(this).attr("max-length") : 0;
        var curlength = $(this).val().length;

        //46 delete
        //8 backspace
        //37 left arrow
        //39 right arrow
        //35 end
        //9 tab
        //190 dot

        if (((charCode != 8 && charCode != 9 && charCode != 35 && charCode != 37 && charCode != 39 && charCode != 46 && charCode != 190) && (charCode < 48 || charCode > 57)) || ((charCode != 8 && charCode != 9 && charCode != 35 && charCode != 37 && charCode != 39 && charCode != 46 && charCode != 190) && (maxlength > 0 && curlength >= maxlength))) {
            return false;
        }
        return true;
    });
});

// Create Base64 Object
var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

/*
$("input[type=text].alphanumeric").on("keypress", function(event) {
        evt = (event) ? event : window.event;			
        var charCode = (evt.which) ? evt.which : evt.keyCode;

        var maxlength = typeof($(this).attr('max-length')) != "undfined" ? $(this).attr("max-length") : 0;
        var curlength = $(this).val().length;

        //46 delete
        //8 backspace
        //37 left arrow
        //39 right arrow
        //35 end
        //9 tab
        //190 dot

        if (((charCode != 8 && charCode != 9 && charCode != 35 && charCode != 37 && charCode != 39 && charCode != 46 && charCode != 190) && (charCode < 48 || charCode > 57)  && (charCode < 65 || charCode > 122)) || ((charCode != 8 && charCode != 9 && charCode != 35 && charCode != 37 && charCode != 39 && charCode != 46 && charCode != 190) && (maxlength > 0 && curlength >= maxlength))) {
                return false;
        }
        return true;
});	
*/
