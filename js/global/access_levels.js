function checke_access_permissions(current_role, needed_role) {
	if(current_role != needed_role) {
		redirect_to_appropriate_page(current_role);
	}
}

function redirect_to_appropriate_page(role) {
	var type;
	
	switch(role) {			
		case '1':
			type = "officer";
			break;
		case '2':
			type = "assistant-officer";
			break;
		case '3':
			type = "security";
			break;
		case '4':
			type = "dispatch";
			break;
		case '5':
			type = "visiting-officer";
			break;
		case '0':					
			type = "admin";
			break;
		default:
			type = "enduser";
	}
	
	window.location.href = hostName+"/"+type+"/myaccount.html";
}

var userobj = sessionStorage.getItem('user');
var sessionTimeOut = sessionStorage.getItem('sessionTimeOut');

if (userobj != null || userobj!= undefined) {    
    setInterval(function(){
        if(sessionTimeOut > 0) {
            sessionTimeOut = sessionTimeOut - 1000;         
            sessionStorage.setItem('sessionTimeOut', sessionTimeOut);
            //console.log("session time left: "+sessionTimeOut);
        } else {
            if (sessionTimeOut != null || sessionTimeOut!= undefined)
                checksessionTimeOut();
        }
    }, 1000);
}

function checksessionTimeOut(){
    sessionStorage.removeItem('sessionTimeOut');
    sessionStorage.removeItem('user');
    alert("Your session is timedout.");
    location.reload();
}

$(document).ready(function(){
    var userobj = sessionStorage.getItem('user');
    userobj = JSON.parse(userobj);
    
    if (userobj != null || userobj!= undefined) {         
        $('.right-nav').prepend('<div class="welcomediv">Welcome '+userobj.gp_usr_emp_name+'</div>');    
    } else {
        $('.welcomediv').remove();
    }
});