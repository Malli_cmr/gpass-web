
function newRequest(){
  
  $('#newreqfrom').removeClass('showitem');
  $('#apmntsall').addClass('showitem');
  $('#myaccount').addClass('showitem'); 
  $('#reports_section').addClass('showitem');
  $('#pageheader .titlespan').text('New Request'); 
 
  $("#errmsg").text('');
  
}

$(document).ready(function(){
	$(".right-nav ul li").on("click tap", function(){
		$(".right-nav ul li.active").removeClass('active');
		$(this).addClass('active');	
	});

	$("#appointments").on("click tap", function(){		
		pendingapprovals();
	});
	
	$("#newrequest").on("click tap", function(){		
		newRequest();
	});
	
	$("#myprofile").on("click tap", function(){		
		myaccount();
	});
	
	$("#logout").on("click tap", function(){		
		logoutaction();
	});
});



function myaccount() {
	var user = JSON.parse(sessionStorage.getItem('user'));
	$("#reloadscript").remove();
	$("#errmsg").text('');
	
	$('#myac_gp_usr_emp_name').val(user.gp_usr_emp_name);
    $('#myac_gp_usr_emp_emailid').val(user.gp_usr_emp_emailid);
    $('#myac_gp_usr_emp_mobileno').val(user.gp_usr_emp_mobileno);
    $('#myac_gp_usr_emp_passwd').val('');
    $('#myac_gp_usr_emp_deptmnt').val(user.gp_usr_emp_deptmnt);
    $('#myac_gp_usr_emp_designation').val(user.gp_usr_emp_designation);
    $('#myac_gp_usr_emp_addtional_info').val(user.gp_usr_emp_addtional_info);
    		
	$('#newreqfrom').addClass('showitem');
	$('#apmntsall').addClass('showitem');
  	$('#myaccount').removeClass('showitem');  
        $('#reports_section').addClass('showitem');
  	$('#pageheader .titlespan').text('My Profile'); 	
}

function pendingapprovals(){
  $("#errmsg").text('');
  $('#newreqfrom').addClass('showitem');
  $('#myaccount').addClass('showitem'); 
  $('#reports_section').addClass('showitem');
  $('#pageheader .titlespan').text('Approvals'); 
  function getallapmnts(){
      
      drawPendingAppointmentsTable();
    drawIgnoredAppointmentsTableMultiple();
    return false;
    
    var userobject = sessionStorage.getItem('user');
      var userfobj = JSON.parse(userobject);
      
    $.ajax({
      type: "GET",
      url:hostName+"/api/apmnt/offtoapprove/"+userfobj.gp_usr_id+"/"+userfobj.gp_usr_emp_proxy_for,
      async: false,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var apmntdata = succdata.data;
          $('#apmntlist').empty();
          $('#apmntlist2').empty();
         $.each(apmntdata, function(i, reqname){
             var pplnames = reqname.gp_visitor_accomp_persons;
         pplnames = pplnames.substr(pplnames.indexOf(",") + 1);
             var vehiclenos = reqname.gp_visitor_vehicle_no;
         vehiclenos = vehiclenos.substr(vehiclenos.indexOf(",") + 1);
         if(reqname.gp_officer_approval_flag == 0){
           
         
         $('#apmntlist').append('<tr id="'+reqname.gp_id+'"><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+reqname.gp_id+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+reqname.gp_visitor_name+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+pplnames+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+vehiclenos+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+reqname.gp_tomeet_from+'</a></td></tr>'); 
         }else if(reqname.gp_officer_approval_flag == 2){
           
           $('#apmntlist2').append('<tr id="'+reqname.gp_id+'"><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+reqname.gp_id+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+reqname.gp_visitor_name+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+pplnames+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+vehiclenos+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+reqname.gp_tomeet_from+'</a></td></tr>');
         }
        
            });
                  
        }
      }
    
  
  });
  
  
    
  };
  
  getallapmnts();
  $('#apmntsall').removeClass('showitem');
  
  window.setTimeout(function(){if(!$("#apmntsall").is(':hidden') && $(".modal").is(':hidden'))$("#appointments").trigger('tap');}, 30000);
  
}

var pid;

function fetchidinfo (sender) {
	$("#errmsg").text('');
    var t = sender.parentNode.parentNode;
    pid = t.getAttribute('id'); 

  $.ajax({
      type: "GET",
      url:hostName+"/api/apmnt/"+pid,
      async: false,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var apmntdata = succdata.data;
         var entrytype ; 
          if(apmntdata.gp_visitor_apmnt_options==1){
            entrytype ="Single";
          }else if(apmntdata.gp_visitor_apmnt_options==2){
            entrytype ="Multiple";
          }
         var noofvehicle = apmntdata.gp_visitor_vehicle_no;
         var vehiclenos = apmntdata.gp_visitor_vehicle_no;
         vehiclenos = vehiclenos.substr(vehiclenos.indexOf(",") + 1);
            noofvehicle = noofvehicle.split(",");
       var noofppl = apmntdata.gp_visitor_accomp_persons;
       var pplnames = apmntdata.gp_visitor_accomp_persons;
         pplnames = pplnames.substr(pplnames.indexOf(",") + 1);
          noofppl = noofppl.split(",");
          var fnoppl = Number(noofppl[0]) ;
        $('#apmntdetailinfo').empty();
        
        $('#apmntdetailinfo').append('<tr><td>Visiting Id </td><td id="#apmntidval">'+apmntdata.gp_id+'</td></tr><tr><td>Entry Type</td><td>'+entrytype+'</td></tr><tr><td>Entry From Time</td><td>'+apmntdata.gp_tomeet_from+'</td></tr><tr><td>Entry To Time</td><td>'+apmntdata.gp_tomeet_to+'</td></tr><tr><td>Visiting Officer Name</td><td>'+apmntdata.gp_tomeet_name+'</td></tr><tr><td>Visitor Name</td><td>'+apmntdata.gp_visitor_name+'</td></tr><tr><td>Visitor Email id</td><td>'+apmntdata.gp_visitor_email+'</td></tr><tr><td>Visitor Mobile No</td><td>'+apmntdata.gp_visitor_mobile_no+'</td></tr><tr><td>Visitor ID Proof</td><td>'+apmntdata.gp_visitor_idproof+'</td></tr><tr><td>Visitor Address</td><td>'+apmntdata.gp_visitor_address+'</td></tr><tr><td>No of Vehicles</td><td><input type="text" id="'+apmntdata.gp_id+'novehicle"value="'+noofvehicle[0]+'"</td></tr><tr><td>Visitor Vehicle No(s)</td><td><input type="text" id="'+apmntdata.gp_id+'vehiclenos"value="'+vehiclenos+'"</td></tr><tr><td>No of Accompanying persons </td><td><input type="text" id="'+apmntdata.gp_id+'noppl"value="'+fnoppl+'"></td></tr><tr><td>Visitor Accompanying persons</td><td><input type="text" id="'+apmntdata.gp_id+'names"value="'+pplnames+'" ></td></tr></td></tr><tr><td>Purpose of Visit</td><td>'+apmntdata.gp_visitor_notes+'</td></tr><tr><td>Notes to Security Officer</td><td> <input type="text" id="'+apmntdata.gp_id+'ofcnotes"value="'+apmntdata.gp_officer_approval_notes+'"></td></tr>');
            }}});
 
  
  
}

function fetchidinfo2 (sender) {
	$("#errmsg").text('');
    var t = sender.parentNode.parentNode;
    pid = t.getAttribute('id'); 

  $.ajax({
      type: "GET",
      url:hostName+"/api/apmnt/"+pid,
      async: false,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var apmntdata = succdata.data;
         var entrytype ; 
          if(apmntdata.gp_visitor_apmnt_options==1){
            entrytype ="Single";
          }else if(apmntdata.gp_visitor_apmnt_options==2){
            entrytype ="Multiple";
          }
         var noofvehicle = apmntdata.gp_visitor_vehicle_no;
         var vehiclenos = apmntdata.gp_visitor_vehicle_no;
         vehiclenos = vehiclenos.substr(vehiclenos.indexOf(",") + 1);
            noofvehicle = noofvehicle.split(",");
       var noofppl = apmntdata.gp_visitor_accomp_persons;
       var pplnames = apmntdata.gp_visitor_accomp_persons;
         pplnames = pplnames.substr(pplnames.indexOf(",") + 1);
          noofppl = noofppl.split(",");
          var fnoppl = Number(noofppl[0]) ;
        $('#reportsapmntdetailinfo').empty();
        
    $('#reportsapmntdetailinfo').append('<tr><td>Visiting Id </td><td id="#apmntidval">'+apmntdata.gp_id+'</td></tr><tr><td>Entry Type</td><td>'+entrytype+'</td></tr><tr><td>Entry From Time</td><td>'+apmntdata.gp_tomeet_from+'</td></tr><tr><td>Entry To Time</td><td>'+apmntdata.gp_tomeet_to+'</td></tr><tr><td>Visiting Officer Name</td><td>'+apmntdata.gp_tomeet_name+'</td></tr><tr><td>Visitor Name</td><td>'+apmntdata.gp_visitor_name+'</td></tr><tr><td>Visitor Email id</td><td>'+apmntdata.gp_visitor_email+'</td></tr><tr><td>Visitor Mobile No</td><td>'+apmntdata.gp_visitor_mobile_no+'</td></tr><tr><td>Visitor ID Proof</td><td>'+apmntdata.gp_visitor_idproof+'</td></tr><tr><td>Visitor Address</td><td>'+apmntdata.gp_visitor_address+'</td></tr><tr><td>No of Vehicles</td><td><input type="text" id="'+apmntdata.gp_id+'novehicle"value="'+noofvehicle[0]+'"</td></tr><tr><td>Visitor Vehicle No(s)</td><td><input type="text" id="'+apmntdata.gp_id+'vehiclenos"value="'+vehiclenos+'"</td></tr><tr><td>No of Accompanying persons </td><td><input type="text" id="'+apmntdata.gp_id+'noppl"value="'+fnoppl+'"></td></tr><tr><td>Visitor Accompanying persons</td><td><input type="text" id="'+apmntdata.gp_id+'names"value="'+pplnames+'" ></td></tr></td></tr><tr><td>Purpose of Visit</td><td>'+apmntdata.gp_visitor_notes+'</td></tr><tr><td>Notes to Security Officer</td><td> <input type="text" id="'+apmntdata.gp_id+'ofcnotes"value="'+apmntdata.gp_officer_approval_notes+'"></td></tr>');
            }}});
 
  
  
}

function declinebyofc(){
  $("#errmsg").text('');
  var params;
     var registerdata;
     var enttype = $( '#etype' ).val();
     var fromdate = $('#fromdate').val();
     var fromtime = $('#fromtime').val();
     var  ffrom = fromdate+" "+fromtime+":00" ;
     var todate = $('#todate').val();
     var totime = $('#totime').val();
     var  fto = todate+" "+totime+":00" ;  
     var ofcnotes = pid+"ofcnotes";
   params = {
                  "gp_officer_approval_flag": "2",
                  "gp_officer_approval_notes": document.getElementById(ofcnotes).value,
                  "gp_security_approval_flag": "0",
                  "gp_security_approval_notes": "secappnotes",
                  "gp_gkeep_dispatch_count": 0,
                  "gp_apmnt_last_visit": "2014-12-20 06:16:14"
                  
                };

  registerdata = JSON.stringify(params);
  var apmntidval = $('#apmntidval').text();
  
  $.ajax({
      type: "PUT",
      url:hostName+"/api/apmnt/updateofcflag/"+pid,
      async: false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var alertdata = succdata.data;
         var msg = "Your Gate Pass request ignored   "+alertdata.gp_id;
        
         
         alert(msg);
         pendingapprovals();
         $('#popup-basic').modal('hide');
                  
        }
      },
      });
   

  
  
}


function approvebyofc(){
  $("#errmsg").text('');
  var params;
     var registerdata;
     var enttype = $( '#etype' ).val();
     var fromdate = $('#fromdate').val();
     var fromtime = $('#fromtime').val();
     var  ffrom = fromdate+" "+fromtime+":00" ;
     var todate = $('#todate').val();
     var totime = $('#totime').val();
     var  fto = todate+" "+totime+":00" ;  
     var ofcnotes = pid+"ofcnotes";
     var vstr1 = pid+"novehicle";
     var vstr2 = pid+"vehiclenos";
     var fvstr = document.getElementById(vstr1).value+","+document.getElementById(vstr2).value ;
     var pstr1 = pid+"noppl";
     var pstr2 = pid+"names";
     var fpstr = document.getElementById(pstr1).value+","+document.getElementById(pstr2).value ;
         
   params = { 
                  "gp_visitor_vehicle_no": fvstr ,
                  "gp_visitor_accomp_persons": fpstr,
                  "gp_officer_approval_flag": "1",
                  "gp_officer_approval_notes":document.getElementById(ofcnotes).value,
                  "gp_security_approval_flag": "1",
                  "gp_security_approval_notes": "secappnotes",
                  "gp_gkeep_dispatch_count": 0,
                  "gp_apmnt_last_visit": "2014-12-20 06:16:14"
                  
                };

  registerdata = JSON.stringify(params);
  var apmntidval = $('#apmntidval').text();
  
  $.ajax({
      type: "PUT",
      url:hostName+"/api/apmnt/updatevisitinfo/"+pid,
      async: false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var alertdata = succdata.data;
         var msg = "Your Gate Pass request forwarded successfully and please note the following gate pass id "+alertdata.gp_id+" for your reference";
         
         
         //alert(msg);
         pendingapprovals();
         $('#popup-basic').modal('hide');
         
        }
      },
      });
   

  
  
}

function reqtypesubmit(){
	$("#errmsg").text('');
  console.log(document.getElementById("gpreq").value);
  if(document.getElementById("gpreq").value=0){
    
    alert('please select the request type');
  }else if(document.getElementById("gpreq").value=1){
     $('#smsreq').removeClass('showitem');
      $('#emailreq').addClass('showitem');
      $('#selfform').addClass('showitem');
    
  }else if(document.getElementById("gpreq").value=2){
    $('#selfform').addClass('showitem');
    $('#smsreq').addClass('showitem');
      $('#emailreq').removeClass('showitem');
  }else if(document.getElementById("gpreq").value=3){
    
    $('#smsreq').addClass('showitem');
      $('#emailreq').addClass('showitem');
      $('#selfform').removeClass('showitem');
  }
  
  
}

function apmntreqsubmit(){
    $("#errmsg").text('');
    var userobject = sessionStorage.getItem('user');
      var userfobj = JSON.parse(userobject);
      
   var vstr ="";
   var pstr ="";
   for(i=1; i<=document.getElementById("vehnos").value; i++){
          
      var str1 = "vehicalNumber"+i ;
      if(document.getElementById(str1).value.length ==0 ){
        alert("please enter all vehicle nos");
      }else{
     vstr = vstr +document.getElementById(str1).value+","; 
     }
   }
   for(i=1; i<=document.getElementById("personsnumber").value; i++){
      var str1 = "pnames"+i ;
     if(document.getElementById(str1).value.length ==0 ){
        alert("please enter all Persons names");
      }else{
     pstr = pstr +document.getElementById(str1).value+","; 
     }
     
   }
    
    
    $(".errline").removeClass('errline');
    
    if(document.getElementById("fullName").value.length==0)
    {
        
        $('#errmsg').empty();
        $('#errmsg').text('Please enter Fullname');
        
        $("#fullName").addClass('errline');
        window.location.href="#";
        
        
    }else if(document.getElementById("phoneNumber").value.length<10)
    {
       $('#errmsg').empty();
        $('#errmsg').text('Please enter Phone Number');
        
         $("#phoneNumber").addClass('errline');
         window.location.href="#";
        


    }else if(document.getElementById("email").value!="" && !emailre.test(document.getElementById("email").value))
    {
        $('#errmsg').empty();
        $('#errmsg').text('Please enter valid Email');
        
        $("#email").addClass('errline');
        window.location.href="#";
         
    }else if(document.getElementById("fromdate").value == ''  )
    {
       $('#errmsg').empty();
        $('#errmsg').text('Please enter From Date & Time');
        
        $("#fromdate").addClass('errline');
         window.location.href="#";
        
    }else if($( '#etype' ).val() == 2 && document.getElementById("todate").value == ''  )
    {
       $('#errmsg').empty();
        $('#errmsg').text('Please enter To Date & Time');
        
        $("#todate").addClass('errline');
         window.location.href="#";
        
    }else if($( '#etype' ).val() == 2 && document.getElementById("todate").value < document.getElementById("fromdate").value  )
    {
       $('#errmsg').empty();
        $('#errmsg').text('Please enter To time after  from time');
        
        $("#todate").addClass('errline');
         window.location.href="#";
        
    }else if(vstr.length==0 && document.getElementById("vehnos").value >0)
    {
      $('#errmsg').empty();
      $('#errmsg').text('Please enter vehicle Nos');
      
       $("#vehnos").addClass('errline');
         window.location.href="#";
        
    }else  if(pstr.length ==0 && document.getElementById("personsnumber").value >0)
    {
      $('#errmsg').empty();
      $('#errmsg').text('Please enter Person name(S) minimum of 5 characters');
      
      $("#personsnumber").addClass('errline');
         window.location.href="#";
        
    }else if(document.getElementById("purpose").value =='' )
    {
      $('#errmsg').empty();
      $('#errmsg').text('Please select purpose of visit');
      
        $("#purpose").addClass('errline');
         window.location.href="#";
        
    }else if(document.getElementById("gp_visitor_address").value.length <= 9 )
    {
      $('#errmsg').empty();
      $('#errmsg').text('Please enter address minimum of 10 characters');
         window.location.href="#";
        
    }else 
    {
      var params;
     var registerdata;
     var enttype = $( '#etype' ).val();
     var fromdate = $('#fromdate').val();
     var  ffrom = fromdate+":00" ;
     var totime = $('#todate').val();
     
     if(totime != '')
        var  fto = totime+":00" ;
    else
        var  fto = "00-00-0000 00:00:00" ;  
 
     if(document.getElementById("vehnos").value > 0)
     var vehiclestr = document.getElementById("vehnos").value+','+vstr ;
     else
     var vehiclestr = '';
     
     if(document.getElementById("vehnos").value > 0)
     var peoplestr = document.getElementById("personsnumber").value+','+pstr;
     else
     var peoplestr = '';
 
   params = {
                 
                  "gp_visiting_id": "",
                  "gp_tomeet_name":userfobj.gp_usr_emp_name,
                  "gp_tomeet_email": userfobj.gp_usr_emp_emailid,
                  "gp_tomeet_mobile_no": userfobj.gp_usr_emp_mobileno,
                  "gp_tomeet_from": ffrom,
                  "gp_tomeet_to": fto,
                  "gp_visitor_name": document.getElementById("fullName").value,
                  "gp_visitor_email": document.getElementById("email").value,
                  "gp_visitor_mobile_no": document.getElementById("phoneNumber").value,
                  "gp_visitor_vehicle_no": vehiclestr,
                  "gp_visitor_accomp_persons": peoplestr,
                  "gp_visitor_notes": document.getElementById("purpose").value,
                  "gp_visitor_photo_url": "vistorphotourl",
                  "gp_visitor_apmnt_options": enttype,
                  "gp_officer_approval_flag": "1",
                  "gp_officer_approval_notes": "offappnotes",
                  "gp_security_approval_flag": "1",
                  "gp_security_approval_notes": "secappnotes",
                  "gp_gkeep_dispatch_count": "0",
                  "gp_apmnt_last_visit": "",
                  "gp_usr_id":userfobj.gp_usr_id,
                  "gp_visitor_idproof": document.getElementById("gp_visitor_idproof").value,
                  "gp_visitor_address": document.getElementById("gp_visitor_address").value
                };


  registerdata = JSON.stringify(params);

  $.ajax({
      type: "POST",
      url:hostName+"/api/apmnt",
      async: false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var alertdata = succdata.data;
         var msg = "Your Gate Pass request is approved. Visitors can visit at the requested time. Please note the Gate Pass id "+alertdata.gp_id+" for your reference";
         
          
          
           $('#fullName').val('');
           $('#phoneNumber').val('');
           $('#email').val('');
           $('#fromdate').val('');
           $('#todate').val('');
           $('#etype').val('');
           $('#vehnos').val('');
           $('#personsnumber').val('');
           $('#purpose').val('');
           $('#pic').val('');
           $('#purposeNotes').val('');
           $('#gp_visitor_idproof').val('');
           $('#gp_visitor_address').val('');
           
           for(i=1; i<=document.getElementById("vehnos").value; i++){          
			  	var str1 = "vehicalNumber"+i ;
			  	document.getElementById(str1).remove();
   			}
   			for(i=1; i<=document.getElementById("personsnumber").value; i++){
      			var str1 = "pnames"+i ;
     			document.getElementById(str1).remove();        
   			}
   			
   			 $('#modalmsg').empty();
          $('#modalmsg').text(msg);
          $('#messagemodal').modal('show');	 
            
            var subject = "AP Secretartiate Gate Pass Request Status";
            var smstext = "Your Gate Pass request with gate pass id "+alertdata.gp_id+" has been Approved. Plz undergo gate security check. Thanks.";
            var mailtext = "Hi <br /> Your Gate Pass request with gate pass id "+alertdata.gp_id+" has been approved. Please undergo gate security check. <br /> Thanks, <br/> Security Team </br> AP Secretariate";

            var mobilenos = alertdata.gp_visitor_mobile_no+",91"+alertdata.gp_tomeet_mobile_no;
            
            gpassnotification.sms(mobilenos, smstext);
            gpassnotification.email(alertdata.gp_tomeet_email, subject, mailtext);
            gpassnotification.email(alertdata.gp_visitor_email, subject, mailtext);
           
          pendingapprovals();
          
           
         
                  
        }
      },
      });
   
  }
  
};

function smsreqsubmit(){
  $("#errmsg").text('');
    var userobject = sessionStorage.getItem('user');
      var userfobj = JSON.parse(userobject);
  
  if(document.getElementById("reqmobileno").value.length == 0){

            $('#modalmsg').empty();
          $('#modalmsg').text('Please Enter Mobile No');
           $('#messagemodal').modal('show');
  }else if(document.getElementById("reqmobileno").value.length != 10){
     $('#modalmsg').empty();
          $('#modalmsg').text('Please Enter valid 10 digit Mobile No');
           $('#messagemodal').modal('show');
     
  }else{
    var params;
     var registerdata;
     
     // Decode the userID
    var userID = Base64.encode(userfobj.gp_usr_id);
    
    // Decode the mbl
    var mbl = Base64.encode(document.getElementById("reqmobileno").value);
    
   params = {
                 
  "mobileno":"91"+document.getElementById("reqmobileno").value,
  "formurl":hostName+"/enduser/index.html?id="+userID+"&mbl="+mbl

                  
                };

  registerdata = JSON.stringify(params);
  
  $.ajax({
      type: "POST",
      url:hostName+"/api/apmnt/newreqsms",
      async: false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var alertdata = succdata.status;
      
         $('#modalmsg').empty();
          $('#modalmsg').text('Your Gate Pass request submitted successfully');
          $('#reqmobileno').val('');
           $('#messagemodal').modal('show');
            pendingapprovals();

         
                  
        }
      },
      });

    
  }
  
}

function emailreqsubmit(){
  $("#errmsg").text('');
  var userobject = sessionStorage.getItem('user');
      var userfobj = JSON.parse(userobject);
      
  if(document.getElementById("reqemailid").value.length == 0){
    
        $('#modalmsg').empty();
         $('#modalmsg').text('Please Enter emailid');
        $('#messagemodal').modal('show');
         

    
  }else {
    var params;
     var registerdata;
     
     // Encode the userID
    var userID = Base64.encode(userfobj.gp_usr_id);
    
    // Decode the email
    var reqEmail = Base64.encode(document.getElementById("reqemailid").value);
    
   params = {
                 
  "email":document.getElementById("reqemailid").value,
  "formurl": hostName+"/enduser/index.html?id="+userID+"&mail="+reqEmail

                  
                };

  registerdata = JSON.stringify(params);
  
  $.ajax({
      type: "POST",
      url:hostName+"/api/apmnt/newreqmail",
      async: false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var alertdata = succdata.data;
       
         
         
    $('#modalmsg').empty();
         $('#modalmsg').text('Your Gate Pass request submitted successfully');
        $('#messagemodal').modal('show');
         pendingapprovals();
                  
        }
      },
      });

    
  }
  
}

//Reports related scripts

$(document).ready(function(){    
    $('#showreports').on('click', function(){ 
        $('#pageheader .titlespan').text('Reports');
   
        $("#errmsg").text('');
  
        $('#newreqfrom').addClass('showitem');
        $('#apmntsall').addClass('showitem');
        $('#myaccount').addClass('showitem');         
        $('#reports_section').removeClass('showitem');
        drawtable();
    });

    $("#search_data_btn").on("click", function() {
        if($("#search_fromdate").val() == '') {
            alert("Please enter from date");
            return false;
        } else if($("#search_todate").val() == '') {
            alert("Please enter to date");
            return false;
        }
        
        drawtable();
    });

    $('#search_todate').datetimepicker({
        timepicker:false,
        format:'d-m-Y',
        //minDate:'0',
        minTime: '09:00',
        maxTime: '19:00'
    });

    $('#search_fromdate').datetimepicker({
        timepicker:false,
        format:'d-m-Y',
        //minDate:'0',
        minTime: '09:00',
        maxTime: '19:00'
    });

    $("#export_data_btn").click(function (e) {  
        if($("#search_fromdate").val() == '') {
            alert("Please enter from date");
            return false;
        } else if($("#search_todate").val() == '') {
            alert("Please enter to date");
            return false;
        }        
        
        var userobject = sessionStorage.getItem('user');
        var userfobj = JSON.parse(userobject);
      
        $.ajax({
            type: "POST",
            url: hostName+"/api/apmnt/searchappointmentsbydate",
            async: false,
            "dataType": "json",
            "data" : {"sdate": $("#search_fromdate").val(), 
                    "edate": $("#search_todate").val(),
                    "user_id": userfobj.gp_usr_id,
                    "export": "csv"},
            headers:{
                "Content-Type":"application/json",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
            success: function (file){ 
                if(file.name != "null"){                                        
                    location.href = hostName+"/api/download.php?file="+file.name;
                }
            }
        });                            
    });

    $("#print_data_btn").click(function (e) {
        if($("#search_fromdate").val() == '') {
            alert("Please enter from date");
            return false;
        } else if($("#search_todate").val() == '') {
            alert("Please enter to date");
            return false;
        }
        
        drawtable();
        printreport();
        e.preventDefault();
    });

    function drawtable() {
        var userobject = sessionStorage.getItem('user');
        var userfobj = JSON.parse(userobject);
        
        if ($.fn.dataTable.isDataTable('#reports_section_table')) {
            $('#reports_section_table').DataTable({
                "destroy": true,
                "processing": true,
                "serverSide": false,                            
                "ajax": {
                    "url": hostName+"/api/apmnt/searchappointmentsbydate",
                    "type": "POST",
                    async: false,
                    dataType: "json",
                    "data" : {"sdate": $("#search_fromdate").val(), 
                            "edate": $("#search_todate").val(),
                            "user_id": userfobj.gp_usr_id},
                    headers:{
                        "Content-Type":"application/json",
                        "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                        "Authorization": bauth,
                    },
                },                            
                "columns": [
                    { "data": "gp_id" },
                    { "data": "gp_visitor_name" },
                    { "data": "gp_visitor_mobile_no" },
                    { "data": "gp_visitor_vehicle_no" },
                    { "data": "gp_visitor_address" },
                    { "data": "gp_tomeet_name" },
                    { "data": "gp_apmnt_last_visit" },
                    { "data": "gp_visitor_accomp_persons" } 
                ],
                "bFilter" : false,    
                "columnDefs": [
				    { "orderable": false, "targets": [4,7] }
			  	],
			  	"order": [[ 0, 'desc' ]],            
                "iDisplayLength": 25,
                "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, 'All']],
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).attr("id", aData['gp_id']);                    
                    $('td', nRow).contents().wrap('<a onclick="fetchidinfo2(this)" data-toggle="modal" href="#reports-popup-basic" />');
                    return nRow;
                }
            });
        } else {
            $('#reports_section_table').DataTable({
                "processing": true,
                "serverSide": false,                            
                "ajax": {
                    "url": hostName+"/api/apmnt/searchappointmentsbydate",
                    "type": "POST",
                    "async": false,
                    "dataType": "json",
                    "data" : {"sdate": $("#search_fromdate").val(), 
                            "edate": $("#search_todate").val(),
                            "user_id": userfobj.gp_usr_id},
                    headers:{
                        "Content-Type":"application/json",
                        "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                        "Authorization": bauth,
                    },
                },                            
                "columns": [
                    { "data": "gp_id" },
                    { "data": "gp_visitor_name" },
                    { "data": "gp_visitor_mobile_no" },
                    { "data": "gp_visitor_vehicle_no" },
                    { "data": "gp_visitor_address" },
                    { "data": "gp_tomeet_name" },
                    { "data": "gp_apmnt_last_visit" },
                    { "data": "gp_visitor_accomp_persons" } 
                ],
                "bFilter" : false,
                "columnDefs": [
				    { "orderable": false, "targets": [4,7] }
			  	],
			  	"order": [[ 0, 'desc' ]],                
                "iDisplayLength": 25,
                "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, 'All']],
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).attr("id", aData['gp_id']);                    
                    $('td', nRow).contents().wrap('<a onclick="fetchidinfo2(this)" data-toggle="modal" href="#reports-popup-basic" />');
                    return nRow;
                }
            });
        }
    }

    function printreport()
    { 
        var disp_setting="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
        disp_setting+="scrollbars=yes,width=1000, height=700, left=100, top=25"; 

        var body, head, content, title;                            

        body = '<table width="100%" cellpadding="0" cellspacing="0">'+
                $("table#reports_section_table > thead").html()+
                $("table#reports_section_table > tbody").html()+
                '</table>';                                                         

        title = "Appointment reports from: "+
                $("#search_fromdate").val()+
                " to: "+$("#search_todate").val();

        var head = "<title>" +                                         
                    title + 
                    "</title>" +
                    "<link href='../css/header.css' rel='stylesheet' />" +
                    "<style type='text/css'>" +
                    "*{margin: 0;padding: 0;} \n\
                    b{font-family: Arial,Helvetica,sans-serif; font-size: 12px;} \n\
                    body{color: #181818; font-family: 'Exo',Helvetica,Arial,sans-serif; font-size: 12px; padding:5px;} \n\
                    th{font-size: 11px;color: #000;font-weight: bold;background-color: #ccc;text-align:left;} \n\
                    td.altRow{background: #f7f7f7 !important; font-size: 11px;padding-left: 3px;} \n\
                    table{border:none !important;} img{display:none;} \n\
                    td{font-size: 14px;padding-left: 10px;padding-right: 5px;}\n\
                    tr td{border-bottom: 1px solid;padding-top: 4px;}\n\
                    tr th{border-bottom: 1px solid;}\n\
                    td a{color: #000;text-decoration: none;}" +
                    "</style>";

        content = "<html><head>" + 
                    head +
                    "</head>" +
                    "<body>" +
                    body +
                    "</body></html>";

        var docprint=window.open("","",disp_setting); 

        docprint.document.open(); 
        docprint.document.write(content); 
        docprint.document.close(); 
        docprint.focus(); 
        docprint.print(); 
        docprint.close();
    }
});

function drawPendingAppointmentsTable() {
    var userobject = sessionStorage.getItem('user');
    var userfobj = JSON.parse(userobject);
    
    if ($.fn.dataTable.isDataTable('#pendingAppointmentstab')) {
        $('#pendingAppointmentstab').DataTable({
            "destroy": true,
            "processing": true,
            "serverSide": false,                            
            "ajax": {
                url: hostName+"/api/apmnt/offtoapproveApproved/"+userfobj.gp_usr_id+"/"+userfobj.gp_usr_emp_proxy_for,
                type: "GET",
                async: false,
                dataType: "json",                   
                headers:{
                    "Content-Type":"application/json",
                    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                    "Authorization": bauth,
                }
            },                          
            "columns": [
                { "data": "gp_id" },
                { "data": "gp_visitor_name" },
                { "data": "gp_visitor_accomp_persons" } ,
                { "data": "gp_visitor_vehicle_no" },  
                { "data": "gp_tomeet_from" }                  
            ],
            "bFilter" : false,     
            "columnDefs": [
                                { "orderable": false, "targets": [] }
                            ],
                            "order": [[ 0, 'desc' ]],           
            "iDisplayLength": 25,
            "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, 'All']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                var pplnames = $(nRow).find("td:eq(3)").html();                                
                $(nRow).find("td:eq(3)").html(pplnames.substr(pplnames.indexOf(",") + 1));
                
                var vehiclenos = $(nRow).find("td:eq(4)").html();                
                $(nRow).find("td:eq(4)").html(vehiclenos.substr(vehiclenos.indexOf(",") + 1));
                
                $(nRow).attr("id", aData['gp_id']);
                $('td', nRow).contents().wrap('<a onclick="fetchidinfo(this)" data-toggle="modal" href="#popup-basic" />');             
                
                return nRow;
            }
        });
    } else {
        $('#pendingAppointmentstab').DataTable({
            "processing": true,
            "serverSide": false,                            
            "ajax": {
                url: hostName+"/api/apmnt/offtoapproveApproved/"+userfobj.gp_usr_id+"/"+userfobj.gp_usr_emp_proxy_for,
                type: "GET",
                async: false,
                dataType: "json",                   
                headers:{
                    "Content-Type":"application/json",
                    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                    "Authorization": bauth,
                }
            },                            
            "columns": [
                { "data": "gp_id" },
                { "data": "gp_visitor_name" },
                { "data": "gp_visitor_accomp_persons" } ,
                { "data": "gp_visitor_vehicle_no" },  
                { "data": "gp_tomeet_from" } 
            ],
            "bFilter" : false, 
            "columnDefs": [
                                { "orderable": false, "targets": [] }
                            ],
                            "order": [[ 0, 'desc' ]],               
            "iDisplayLength": 25,
            "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, 'All']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {                
                var pplnames = $(nRow).find("td:eq(3)").html();                                
                $(nRow).find("td:eq(3)").html(pplnames.substr(pplnames.indexOf(",") + 1));
                
                var vehiclenos = $(nRow).find("td:eq(4)").html();                
                $(nRow).find("td:eq(4)").html(vehiclenos.substr(vehiclenos.indexOf(",") + 1));
                
                $(nRow).attr("id", aData['gp_id']);
                $('td', nRow).contents().wrap('<a onclick="fetchidinfo(this)" data-toggle="modal" href="#popup-basic" />');
                
                return nRow;
            }                                   
        });
    }
}

function drawIgnoredAppointmentsTableMultiple() {
    var userobject = sessionStorage.getItem('user');
    var userfobj = JSON.parse(userobject);
    
    if ($.fn.dataTable.isDataTable('#ignoredAppointmentstab')) {
        $('#ignoredAppointmentstab').DataTable({
            "destroy": true,
            "processing": true,
            "serverSide": false,                            
            "ajax": {
                url: hostName+"/api/apmnt/offtoapproveIgnored/"+userfobj.gp_usr_id+"/"+userfobj.gp_usr_emp_proxy_for,
                type: "GET",
                async: false,
                dataType: "json",                   
                headers:{
                    "Content-Type":"application/json",
                    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                    "Authorization": bauth,
                }
            },                          
            "columns": [
                { "data": "gp_id" },
                { "data": "gp_visitor_name" },
                { "data": "gp_visitor_accomp_persons" } ,
                { "data": "gp_visitor_vehicle_no" },  
                { "data": "gp_tomeet_from" }                     
            ],
            "bFilter" : false,     
            "columnDefs": [
                                { "orderable": false, "targets": [] }
                            ],
                            "order": [[ 0, 'desc' ]],           
            "iDisplayLength": 25,
            "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, 'All']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                var pplnames = $(nRow).find("td:eq(3)").html();                                
                $(nRow).find("td:eq(3)").html(pplnames.substr(pplnames.indexOf(",") + 1));
                
                var vehiclenos = $(nRow).find("td:eq(4)").html();                
                $(nRow).find("td:eq(4)").html(vehiclenos.substr(vehiclenos.indexOf(",") + 1));
                
                $(nRow).attr("id", aData['gp_id']);
                $('td', nRow).contents().wrap('<a onclick="fetchidinfo(this)" data-toggle="modal" href="#popup-basic" />');
                
                return nRow;
            }
        });
    } else {
        $('#ignoredAppointmentstab').DataTable({
            "processing": true,
            "serverSide": false,                            
            "ajax": {
                url: hostName+"/api/apmnt/offtoapproveIgnored/"+userfobj.gp_usr_id+"/"+userfobj.gp_usr_emp_proxy_for,
                type: "GET",
                async: false,
                dataType: "json",                   
                headers:{
                    "Content-Type":"application/json",
                    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                    "Authorization": bauth,
                }
            },                            
            "columns": [
                { "data": "gp_id" },
                { "data": "gp_visitor_name" },
                { "data": "gp_visitor_accomp_persons" } ,
                { "data": "gp_visitor_vehicle_no" },  
                { "data": "gp_tomeet_from" } 
            ],
            "bFilter" : false, 
            "columnDefs": [
                                { "orderable": false, "targets": [] }
                            ],
                            "order": [[ 0, 'desc' ]],               
            "iDisplayLength": 25,
            "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, 'All']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                var pplnames = $(nRow).find("td:eq(3)").html();                                
                $(nRow).find("td:eq(3)").html(pplnames.substr(pplnames.indexOf(",") + 1));
                
                var vehiclenos = $(nRow).find("td:eq(4)").html();                
                $(nRow).find("td:eq(4)").html(vehiclenos.substr(vehiclenos.indexOf(",") + 1));
                
                $(nRow).attr("id", aData['gp_id']);
                $('td', nRow).contents().wrap('<a onclick="fetchidinfo(this)" data-toggle="modal" href="#popup-basic" />');
                
                return nRow;
            }                                   
        });
    }
}