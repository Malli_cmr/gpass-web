$(document).ready(function(){
	$(".right-nav ul li").on("click tap", function(){
		$(".right-nav ul li.active").removeClass('active');
		$(this).addClass('active');	
	});

	$("#appointments").on("click tap", function(){		
		pendingapprovals();                
	});
			
	$("#myprofile").on("click tap", function(){		
		myaccount();
	});
	
	$("#logout").on("click tap", function(){		
		logoutaction();
	});
});
var apmntdata;
function newRequest(){  
  if(document.getElementById('searchquery').value.trim().length ==0 && document.getElementById('searchquery2').value.trim().length ==0 && document.getElementById('searchquery3').value.trim().length ==0){
          $('#modalmsg').empty();
          $('#modalmsg').text('Please enter search text');
          $('#messagemodal').modal('show');
    
  }else{
  
  $("ul.nav-tabs li").removeClass("active");
  $("ul.nav-tabs li:first").addClass("active");
  $("#single, #multiple").removeClass("active");
  $("#all").addClass("active");
  
  drawAllAppointmentsTable();
  drawAppointmentsTable();
  drawAppointmentsTableMultiple();
  
  return false;
  
  $('#newreqfrom').removeClass('showitem');
  $('#apmntsall').addClass('showitem');
  $('#reports_section').addClass('showitem');
  
  function getsearchappointments(){
   
      
    $.ajax({
      type: "GET",
      url:hostName+"/api/apmnt/globalsearch/"+document.getElementById('searchquery').value,
      async: false,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var apmntdata = succdata.data;
        //alert(apmntdata);
        if(apmntdata==""){
          $('#modalmsg').empty();
          $('#modalmsg').text('No Search Results');
          $('#messagemodal').modal('show');
          
        }else{
                  
          $('#apmntlist3').empty();
         $.each(apmntdata, function(i, reqname){
           var vehiclenos = reqname.gp_visitor_vehicle_no;
              vehiclenos = vehiclenos.substr(vehiclenos.indexOf(",") + 1);
              //noofvehicle = noofvehicle.split(",");    
         //if(reqname.gp_visitor_apmnt_options == 1 && reqname.gp_gkeep_dispatch_count ==0){
         
         $('#apmntlist3').append('<tr id="'+reqname.gp_id+'"><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+reqname.gp_id+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+reqname.gp_visitor_name+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+vehiclenos+'</a></td><td id="'+reqname.gp_id+'"><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">details</a></td></tr>'); 
         //}else if(reqname.gp_visitor_apmnt_options == 2){
           
           //$('#apmntlist2').append('<tr id="'+reqname.gp_id+'"><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+reqname.gp_id+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+reqname.gp_visitor_name+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+reqname.gp_tomeet_from+'</a></td><td id="'+reqname.gp_id+'"><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">details</a></td></tr>');
         //}
        
            });
              }    
        }
      }
    
  
  });
  
  
    
  };
 getsearchappointments();
  }
}

function pendingapprovals(){
  
  $('#newreqfrom').addClass('showitem');
  $('#reports_section').addClass('showitem');
  
  function getallapmnts(){
      $("ul.nav-tabs li").removeClass("active");
  $("ul.nav-tabs li:first").addClass("active");
  $("#single, #multiple").removeClass("active");
  $("#all").addClass("active");
  
      drawAllAppointmentsTable();
   drawAppointmentsTable();
  drawAppointmentsTableMultiple();
  return false;
      
    $.ajax({
      type: "GET",
      url:hostName+"/api/apmntallapproved",
      async: false,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var apmntdata = succdata.data;
          $('#apmntlist').empty();
          $('#apmntlist2').empty();
         $.each(apmntdata, function(i, reqname){
            var pplnames = reqname.gp_visitor_accomp_persons;
         pplnames = pplnames.substr(pplnames.indexOf(",") + 1);
         var vehiclenos = reqname.gp_visitor_vehicle_no;
         vehiclenos = vehiclenos.substr(vehiclenos.indexOf(",") + 1);
            //noofvehicle = noofvehicle.split(",");           
         if(reqname.gp_visitor_apmnt_options == 1 && reqname.gp_gkeep_dispatch_count ==0){
         
         $('#apmntlist').append('<tr id="'+reqname.gp_id+'"><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+reqname.gp_id+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+reqname.gp_visitor_name+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+pplnames+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+vehiclenos+'</a></td><td id="'+reqname.gp_id+'"><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">details</a></td></tr>'); 
         }else if(reqname.gp_visitor_apmnt_options == 2){
           
           $('#apmntlist2').append('<tr id="'+reqname.gp_id+'"><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+reqname.gp_id+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+reqname.gp_visitor_name+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+pplnames+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+vehiclenos+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+reqname.gp_gkeep_dispatch_count+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+reqname.gp_apmnt_last_visit+'</a></td><td id="'+reqname.gp_id+'"><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">details</a></td></tr>');
         }
        
            });
            
           $('#panel-multiple').trigger('click');
           $('#panel-single').trigger('click');
                  
        }
      }
    
  
  });
  
  
    
  };
  
  getallapmnts();
  
  $('#apmntsall').removeClass('showitem');
  
  window.setTimeout(function(){if(!$("#apmntsall").is(':hidden') && $(".modal").is(':hidden'))$("#appointments").trigger('tap');}, 30000);
  
}

function getallapprovedrequests(){
  
  $('#newreqfrom').addClass('showitem');
  $('#reports_section').addClass('showitem');
  
  function getallapmnts2(){
   
      
    $.ajax({
      type: "GET",
      url:hostName+"/api/apmntallapproved",
      async: false,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var apmntdata = succdata.data;
          $('#apmntlist').empty();
          $('#apmntlist2').empty();
         $.each(apmntdata, function(i, reqname){
            var pplnames = reqname.gp_visitor_accomp_persons;
         pplnames = pplnames.substr(pplnames.indexOf(",") + 1);
         var vehiclenos = reqname.gp_visitor_vehicle_no;
         vehiclenos = vehiclenos.substr(vehiclenos.indexOf(",") + 1);
            //noofvehicle = noofvehicle.split(",");           
         
         $('#apmntlist').append('<tr id="'+reqname.gp_id+'"><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+reqname.gp_id+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+reqname.gp_visitor_name+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+pplnames+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+vehiclenos+'</a></td><td id="'+reqname.gp_id+'"><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">details</a></td></tr>'); 
         
        
            });
            
           $('#panel-multiple').trigger('click');
           $('#panel-single').trigger('click');
                  
        }
      }
    
  
  });
  
  
    
  };
  
  getallapmnts2();
  
  $('#apmntsall').removeClass('showitem');
  
}

var pid;

function fetchidinfo (sender) {
    var t = sender.parentNode.parentNode;
    pid = t.getAttribute('id'); 

  $.ajax({
      type: "GET",
      url:hostName+"/api/apmnt/"+pid,
      async: false,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
          apmntdata = succdata.data;
         var entrytype ; 
          if(apmntdata.gp_visitor_apmnt_options==1){
            entrytype ="Single";
          }else if(apmntdata.gp_visitor_apmnt_options==2){
            entrytype ="Multiple";
          }
         var noofvehicle = apmntdata.gp_visitor_vehicle_no;
         var vehiclenos = apmntdata.gp_visitor_vehicle_no;
         vehiclenos = vehiclenos.substr(vehiclenos.indexOf(",") + 1);
            noofvehicle = noofvehicle.split(",");
       var noofppl = apmntdata.gp_visitor_accomp_persons;
       var pplnames = apmntdata.gp_visitor_accomp_persons;
         pplnames = pplnames.substr(pplnames.indexOf(",") + 1);
          noofppl = noofppl.split(",");
        $('#apmntdetailinfo').empty();
        
        $('#apmntdetailinfo').append('<tr><td>Visiting Id </td><td id="#apmntidval">'+apmntdata.gp_id+'</td></tr><tr><td>Entry Type</td><td>'+entrytype+'</td></tr><tr><td>Entry From Time</td><td>'+apmntdata.gp_tomeet_from+'</td></tr><tr><td>Entry To Time</td><td>'+apmntdata.gp_tomeet_to+'</td></tr><tr><td>Visiting Officer Name</td><td>'+apmntdata.gp_tomeet_name+'</td></tr><tr><td>Visitor Name</td><td>'+apmntdata.gp_visitor_name+'</td></tr><tr><td>Visitor Email id</td><td>'+apmntdata.gp_visitor_email+'</td></tr><tr><td>Visitor Mobile No</td><td>'+apmntdata.gp_visitor_mobile_no+'</td></tr><tr><td>Visitor ID Proof</td><td>'+apmntdata.gp_visitor_idproof+'</td></tr><tr><td>Visitor Address</td><td>'+apmntdata.gp_visitor_address+'</td></tr><tr><td>No of Vehicles</td><td>'+noofvehicle[0]+'</td></tr><tr><td>Visitor Vehicle No</td><td>'+vehiclenos+'</td></tr><tr><td>No of persons Visiting</td><td>'+noofppl[0]+'</td></tr><tr><td>Visitor Accompanying persons</td><td>'+pplnames+'</td></tr></td></tr><tr><td>Purpose of Visit</td><td>'+apmntdata.gp_visitor_notes+'</td></tr>');

            }}});
 
  
  
}

function fetchidinfo2 (sender) {
    var t = sender.parentNode.parentNode;
    pid = t.getAttribute('id'); 

  $.ajax({
      type: "GET",
      url:hostName+"/api/apmnt/"+pid,
      async: false,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
          apmntdata = succdata.data;
         var entrytype ; 
          if(apmntdata.gp_visitor_apmnt_options==1){
            entrytype ="Single";
          }else if(apmntdata.gp_visitor_apmnt_options==2){
            entrytype ="Multiple";
          }
         var noofvehicle = apmntdata.gp_visitor_vehicle_no;
         var vehiclenos = apmntdata.gp_visitor_vehicle_no;
         vehiclenos = vehiclenos.substr(vehiclenos.indexOf(",") + 1);
            noofvehicle = noofvehicle.split(",");
       var noofppl = apmntdata.gp_visitor_accomp_persons;
       var pplnames = apmntdata.gp_visitor_accomp_persons;
         pplnames = pplnames.substr(pplnames.indexOf(",") + 1);
          noofppl = noofppl.split(",");
        $('#reportsapmntdetailinfo').empty();
        
        $('#reportsapmntdetailinfo').append('<tr><td>Visiting Id </td><td id="#apmntidval">'+apmntdata.gp_id+'</td></tr><tr><td>Entry Type</td><td>'+entrytype+'</td></tr><tr><td>Entry From Time</td><td>'+apmntdata.gp_tomeet_from+'</td></tr><tr><td>Entry To Time</td><td>'+apmntdata.gp_tomeet_to+'</td></tr><tr><td>Visiting Officer Name</td><td>'+apmntdata.gp_tomeet_name+'</td></tr><tr><td>Visitor Name</td><td>'+apmntdata.gp_visitor_name+'</td></tr><tr><td>Visitor Email id</td><td>'+apmntdata.gp_visitor_email+'</td></tr><tr><td>Visitor Mobile No</td><td>'+apmntdata.gp_visitor_mobile_no+'</td></tr><tr><td>Visitor ID Proof</td><td>'+apmntdata.gp_visitor_idproof+'</td></tr><tr><td>Visitor Address</td><td>'+apmntdata.gp_visitor_address+'</td></tr><tr><td>No of Vehicles</td><td>'+noofvehicle[0]+'</td></tr><tr><td>Visitor Vehicle No</td><td>'+vehiclenos+'</td></tr><tr><td>No of persons Visiting</td><td>'+noofppl[0]+'</td></tr><tr><td>Visitor Accompanying persons</td><td>'+pplnames+'</td></tr></td></tr><tr><td>Purpose of Visit</td><td>'+apmntdata.gp_visitor_notes+'</td></tr>');

            }}});
 
  
  
}

function declinebyofc(){
  
  var params;
     var registerdata;
     var enttype = $( '#etype' ).val();
     var fromdate = $('#fromdate').val();
     var fromtime = $('#fromtime').val();
     var  ffrom = fromdate+" "+fromtime+":00" ;
     var todate = $('#todate').val();
     var totime = $('#totime').val();
     var  fto = todate+" "+totime+":00" ;  
     var ofcnotes = pid+"ofcnotes";
      var secnotes = pid+"secnotes";
   params = {
                  "gp_officer_approval_flag": "1",
                  "gp_officer_approval_notes": document.getElementById(ofcnotes).value,
                  "gp_security_approval_flag": "2",
                  "gp_security_approval_notes": document.getElementById(secnotes).value,
                  "gp_gkeep_dispatch_count": 0,
                  "gp_apmnt_last_visit": "2014-12-20 06:16:14"
                  
                };

  registerdata = JSON.stringify(params);
  var apmntidval = $('#apmntidval').text();
  
  $.ajax({
      type: "PUT",
      url:hostName+"/api/apmnt/updateofcflag/"+pid,
      async: false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var alertdata = succdata.data;
         var msg = "Your Gate Pass request ignored successfully and please note the following gate pass id "+alertdata.gp_id+" for your reference";
         var subject = "AP Secretartiate Gate Pass Request Status";
         var ignoretext =" Your Gate Pass request with gate pass id "+alertdata.gp_id+" has been ignored because of reason "+alertdata.gp_security_approval_notes;
         var mailtext =" Hi <br /> Your Gate Pass request with gate pass id "+alertdata.gp_id+" has been ignored because of reason "+alertdata.gp_security_approval_notes+" <br /> Thanks, <br/> Security Team </br> AP Secretariate";
          pendingapprovals();
          $('#popup-basic').modal('hide');
          $('#loading').removeClass('showitem');
          gpassnotification.sms(alertdata.gp_visitor_mobile_no, ignoretext);
          gpassnotification.sms(alertdata.gp_tomeet_mobile_no, ignoretext);
          gpassnotification.email(alertdata.gp_tomeet_email, subject, mailtext);
          gpassnotification.email(alertdata.gp_visitor_email, subject, mailtext);
          $('#loading').text('Request Sent Successfully');
         $('#loading').addClass('showitem');
                  
        }
      },
      });
   

  
  
}


function approvebyofc(){
  
  var params;
     var registerdata;
     var enttype = $( '#etype' ).val();
     var fromdate = $('#fromdate').val();
     var fromtime = $('#fromtime').val();
     var  ffrom = fromdate+" "+fromtime+":00" ;
     var todate = $('#todate').val();
     var totime = $('#totime').val();
     var  fto = todate+" "+totime+":00" ;  
    var ofcnotes = pid+"ofcnotes";
    var secnotes = pid+"secnotes";
   params = {
                  "gp_officer_approval_flag": "1",
                  "gp_officer_approval_notes": document.getElementById(ofcnotes).value,
                  "gp_security_approval_flag": "1",
                  "gp_security_approval_notes": document.getElementById(secnotes).value,
                  "gp_gkeep_dispatch_count": 0,
                  "gp_apmnt_last_visit": "2014-12-20 06:16:14"
                  
                };

  registerdata = JSON.stringify(params);
  var apmntidval = $('#apmntidval').text();
  
  $.ajax({
      type: "PUT",
      url:hostName+"/api/apmnt/updateofcflag/"+pid,
      async: false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var alertdata = succdata.data;
         var msg = "Your Gate Pass request forwarded successfully and please note the following gate pass id "+alertdata.gp_visiting_id+" for your reference";
         var subject = "AP Secretartiate Gate Pass Request Status";
         var ignoretext =" Your Gate Pass request with gate pass id "+alertdata.gp_id+" has been Approved ";
         var mailtext =" Hi <br /> Your Gate Pass request with gate pass id "+alertdata.gp_id+" has been approved <br /> Thanks, <br/> Security Team </br> AP Secretariate";
         pendingapprovals();
          $('#popup-basic').modal('hide');
          $('#loading').removeClass('showitem');
          gpassnotification.sms(alertdata.gp_visitor_mobile_no, ignoretext);
          gpassnotification.sms(alertdata.gp_tomeet_mobile_no, ignoretext);
          gpassnotification.email(alertdata.gp_tomeet_email, subject, mailtext);
          gpassnotification.email(alertdata.gp_visitor_email, subject, mailtext);
          $('#loading').text('Request Sent Successfully');
         $('#loading').addClass('showitem');
         
        }
      },
      });
   

  
  
}

function dispatch(){
  
  var params;
     var registerdata;
    var dcount = apmntdata.gp_gkeep_dispatch_count;
       dcount = Number(dcount);
       dcount = dcount +1;
   params = {
                  
                  "gp_gkeep_dispatch_count": dcount,
                  "gp_apmnt_last_visit": "2014-12-20 06:16:14"
                  
                };

  registerdata = JSON.stringify(params);
  var apmntidval = $('#apmntidval').text();
  
  $.ajax({
      type: "PUT",
      url:hostName+"/api/apmnt/dispatch/"+pid,
      async: false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var alertdata = succdata.data;
         var msg = "Your Gate Pass request forwarded successfully and please note the following gate pass id "+alertdata.gp_visiting_id+" for your reference";
         var subject = "AP Secretartiate Gate Pass Request Status";
         var ignoretext ="Your Gate Pass request with gate pass id "+alertdata.gp_id+" has been Approved ";
         var mailtext ="Hi <br /> Your Gate Pass request with gate pass id "+alertdata.gp_id+" has been approved <br /> Thanks, <br/> Security Team </br> AP Secretariate";
         pendingapprovals();
          $('#popup-basic').modal('hide');
          $('#modalmsg').empty();
          $('#modalmsg').text('Entry allowed successfully, you can search if you need it again');
          $('#messagemodal').modal('show');
         
         
        }
      },
      });
   

  
  
}

function reqtypesubmit(){
  console.log(document.getElementById("gpreq").value);
  if(document.getElementById("gpreq").value=0){
    
    alert('please select the request type');
  }else if(document.getElementById("gpreq").value=1){
     $('#smsreq').removeClass('showitem');
      $('#emailreq').addClass('showitem');
      $('#selfform').addClass('showitem');
    
  }else if(document.getElementById("gpreq").value=2){
    $('#selfform').addClass('showitem');
    $('#smsreq').addClass('showitem');
      $('#emailreq').removeClass('showitem');
  }else if(document.getElementById("gpreq").value=3){
    
    $('#smsreq').addClass('showitem');
      $('#emailreq').addClass('showitem');
      $('#selfform').removeClass('showitem');
  }
  
  
}

function apmntreqsubmit(){
    
    var userobject = sessionStorage.getItem('user');
      var userfobj = JSON.parse(userobject);
    
    
    if(document.getElementById("fullName").value.length==0)
    {
        
        $('#errmsg').empty();
        $('#errmsg').text('Please enter Fullname');
        window.location.href="#";
        
        
    }else if(document.getElementById("email").value=="" || !emailre.test(document.getElementById("email").value))
    {
        $('#errmsg').empty();
        $('#errmsg').text('Please enter valid Email');
        window.location.href="#";
         
    }else if(document.getElementById("phoneNumber").value.length==0)
    {
       $('#errmsg').empty();
        $('#errmsg').text('Please enter Phone Number');
         window.location.href="#";
        


    }else if(document.getElementById("todate").value < document.getElementById("fromdate").value  )
    {
       $('#errmsg').empty();
        $('#errmsg').text('Please enter To time after  from time');
         window.location.href="#";
        


    }else if(document.getElementById("vehicalNumber").value.length==0 && document.getElementById("vehnos").value >0)
    {
      $('#errmsg').empty();
      $('#errmsg').text('Please enter vehicle Nos');
         window.location.href="#";
        
    }else  if(document.getElementById("pnames").value.length < 5 && document.getElementById("personsnumber").value >0)
    {
      $('#errmsg').empty();
      $('#errmsg').text('Please enter Person name(S) minimum of 5 characters');
         window.location.href="#";
        
    }else if(document.getElementById("purpose").value.length < 5 )
    {
      $('#errmsg').empty();
      $('#errmsg').text('Please enter Purpose minimum of 5 characters');
         window.location.href="#";
        
    }else 
    {
      var params;
     var registerdata;
     var enttype = $( '#etype' ).val();
     var fromdate = $('#fromdate').val();
     var  ffrom = fromdate+":00" ;
     var totime = $('#todate').val();
     var  fto = totime+":00" ;  

     var vehiclestr = document.getElementById("vehnos").value+','+document.getElementById("vehicalNumber").value ;
     var peoplestr = document.getElementById("personsnumber").value+','+document.getElementById("pnames").value;
   params = {
                 
                  "gp_visiting_id": "",
                  "gp_tomeet_name":userfobj.gp_usr_emp_name,
                  "gp_tomeet_email": userfobj.gp_usr_emp_emailid,
                  "gp_tomeet_mobile_no": userfobj.gp_usr_emp_mobileno,
                  "gp_tomeet_from": ffrom,
                  "gp_tomeet_to": fto,
                  "gp_visitor_name": document.getElementById("fullName").value,
                  "gp_visitor_email": document.getElementById("email").value,
                  "gp_visitor_mobile_no": document.getElementById("phoneNumber").value,
                  "gp_visitor_vehicle_no": vehiclestr,
                  "gp_visitor_accomp_persons": peoplestr,
                  "gp_visitor_notes": document.getElementById("purpose").value,
                  "gp_visitor_photo_url": "vistorphotourl",
                  "gp_visitor_apmnt_options": enttype,
                  "gp_officer_approval_flag": "0",
                  "gp_officer_approval_notes": "offappnotes",
                  "gp_security_approval_flag": "0",
                  "gp_security_approval_notes": "secappnotes",
                  "gp_gkeep_dispatch_count": "0",
                  "gp_apmnt_last_visit": "",
                  "gp_user_id":userfobj.gp_usr_id
                };

  registerdata = JSON.stringify(params);
  
  $.ajax({
      type: "POST",
      url:hostName+"/api/apmnt",
      async: false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var alertdata = succdata.data;
         var msg = "Your Gate Pass request submitted successfully and please note the following gate pass id "+alertdata.gp_id+" for your reference";
         
           $('#modalmsg').empty();
          $('#modalmsg').text(msg);
          $('#messagemodal').modal('show');
          
          pendingapprovals();
          
           
         
                  
        }
      },
      });
   
  }
  
};

function smsreqsubmit(){
  
    var userobject = sessionStorage.getItem('user');
      var userfobj = JSON.parse(userobject);
  
  if(document.getElementById("reqmobileno").value.length == 0){

            $('#modalmsg').empty();
          $('#modalmsg').text('Please Enter Mobile No');
           $('#messagemodal').modal('show');
  }else if(document.getElementById("reqmobileno").value.length != 10){
     $('#modalmsg').empty();
          $('#modalmsg').text('Please Enter valid 10 digit Mobile No');
           $('#messagemodal').modal('show');
     
  }else{
    var params;
     var registerdata;
     
    
   params = {
                 
  "mobileno":"91"+document.getElementById("reqmobileno").value,
  "formurl":hostName+"/enduser/index.html?id="+userfobj.gp_usr_id

                  
                };

  registerdata = JSON.stringify(params);
  
  $.ajax({
      type: "POST",
      url:hostName+"/api/apmnt/newreqsms",
      async: false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var alertdata = succdata.status;
      
         $('#modalmsg').empty();
          $('#modalmsg').text('Your Gate Pass request submitted successfully');
          $('#reqmobileno').val('');
           $('#messagemodal').modal('show');
            pendingapprovals();

         
                  
        }
      },
      });

    
  }
  
}

function emailreqsubmit(){
  
  var userobject = sessionStorage.getItem('user');
      var userfobj = JSON.parse(userobject);
      
  if(document.getElementById("reqemailid").value.length == 0){
    
        $('#modalmsg').empty();
         $('#modalmsg').text('Please Enter emailid');
        $('#messagemodal').modal('show');
         

    
  }else {
    var params;
     var registerdata;
     
    
   params = {
                 
  "email":document.getElementById("reqemailid").value,
  "formurl": hostName+"/enduser/index.html?id="+userfobj.gp_usr_id

                  
                };

  registerdata = JSON.stringify(params);
  
  $.ajax({
      type: "POST",
      url:hostName+"/api/apmnt/newreqmail",
      async: false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var alertdata = succdata.data;
       
         
         
    $('#modalmsg').empty();
         $('#modalmsg').text('Your Gate Pass request submitted successfully');
        $('#messagemodal').modal('show');
         pendingapprovals();
                  
        }
      },
      });

    
  }
  
}

//Reports related scripts

$(document).ready(function(){
        
    $('#showreports').on('click', function(){ 
        $('#pageheader .titlespan').text('Reports');
   
        $("#errmsg").text('');
  
        $('#newreqfrom').addClass('showitem');
        $('#apmntsall').addClass('showitem');         
        $('#reports_section').removeClass('showitem');
        drawtable();
    });
    
    $("#search_data_btn").on("click", function() {
        if($("#search_fromdate").val() == '') {
            alert("Please enter from date");
            return false;
        } else if($("#search_todate").val() == '') {
            alert("Please enter to date");
            return false;
        }
        
        drawtable();
    });

    $('#search_todate').datetimepicker({
        timepicker:false,
        format:'d-m-Y',
        //minDate:'0',
        minTime: '09:00',
        maxTime: '19:00'
    });

    $('#search_fromdate').datetimepicker({
        timepicker:false,
        format:'d-m-Y',
        //minDate:'0',
        minTime: '09:00',
        maxTime: '19:00'
    });

    $("#export_data_btn").click(function (e) {  
        if($("#search_fromdate").val() == '') {
            alert("Please enter from date");
            return false;
        } else if($("#search_todate").val() == '') {
            alert("Please enter to date");
            return false;
        }        

        $.ajax({
            type: "POST",
            url: hostName+"/api/apmnt/searchappointmentsbydate",
            async: false,
            "dataType": "json",
            "data" : {"sdate": $("#search_fromdate").val(), 
                    "edate": $("#search_todate").val(),
                    "user_id": '',
                    "export": "csv"},
            headers:{
                "Content-Type":"application/json",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
            success: function (file){ 
                if(file.name != "null"){                                        
                    location.href = hostName+"/api/download.php?file="+file.name;
                }
            }
        });                            
    });

    $("#print_data_btn").click(function (e) {
        if($("#search_fromdate").val() == '') {
            alert("Please enter from date");
            return false;
        } else if($("#search_todate").val() == '') {
            alert("Please enter to date");
            return false;
        }
        
        drawtable();
        printreport();
        e.preventDefault();
    });

    function drawtable() {
        if ($.fn.dataTable.isDataTable('#reports_section_table')) {
            $('#reports_section_table').DataTable({
                "destroy": true,
                "processing": true,
                "serverSide": false,                            
                "ajax": {
                    "url": hostName+"/api/apmnt/SearchSppointmentsByDateGateSecurity",
                    "type": "POST",
                    async: false,
                    dataType: "json",
                    "data" : {"sdate": $("#search_fromdate").val(), 
                            "edate": $("#search_todate").val()},
                    headers:{
                        "Content-Type":"application/json",
                        "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                        "Authorization": bauth,
                    },
                },                            
                "columns": [
                    { "data": "gp_id" },
                    { "data": "gp_visitor_name" },
                    { "data": "gp_visitor_mobile_no" },
                    { "data": "gp_visitor_vehicle_no" },
                    { "data": "gp_visitor_address" },
                    { "data": "gp_tomeet_name" },
                    { "data": "gp_apmnt_last_visit" },
                    { "data": "gp_visitor_accomp_persons" } 
                ],
                "bFilter" : false,     
                "columnDefs": [
				    { "orderable": false, "targets": [4,7] }
			  	],
			  	"order": [[ 0, 'desc' ]],           
                "iDisplayLength": 25,
                "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, 'All']],
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).attr("id", aData['gp_id']);                    
                    $('td', nRow).contents().wrap('<a onclick="fetchidinfo2(this)" data-toggle="modal" href="#reports-popup-basic" />');
                    return nRow;
                }
            });
        } else {
            $('#reports_section_table').DataTable({
                "processing": true,
                "serverSide": false,                            
                "ajax": {
                    "url": hostName+"/api/apmnt/SearchSppointmentsByDateGateSecurity",
                    "type": "POST",
                    "async": false,
                    "dataType": "json",
                    "data" : {"sdate": $("#search_fromdate").val(), 
                            "edate": $("#search_todate").val()},
                    headers:{
                        "Content-Type":"application/json",
                        "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                        "Authorization": bauth,
                    },
                },                            
                "columns": [
                    { "data": "gp_id" },
                    { "data": "gp_visitor_name" },
                    { "data": "gp_visitor_mobile_no" },
                    { "data": "gp_visitor_vehicle_no" },
                    { "data": "gp_visitor_address" },
                    { "data": "gp_tomeet_name" },
                    { "data": "gp_apmnt_last_visit" },
                    { "data": "gp_visitor_accomp_persons" } 
                ],
                "bFilter" : false, 
                "columnDefs": [
				    { "orderable": false, "targets": [4,7] }
			  	],
			  	"order": [[ 0, 'desc' ]],               
                "iDisplayLength": 25,
                "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, 'All']],
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).attr("id", aData['gp_id']);
                    $('td', nRow).contents().wrap('<a onclick="fetchidinfo2(this)" data-toggle="modal" href="#reports-popup-basic" />');
                    return nRow;
                }                                   
            });
        }
    }
    
    function printreport()
    { 
        var disp_setting="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
        disp_setting+="scrollbars=yes,width=1000, height=700, left=100, top=25"; 

        var body, head, content, title;                            

        body = '<table width="100%" cellpadding="0" cellspacing="0">'+
                $("table#reports_section_table > thead").html()+
                $("table#reports_section_table > tbody").html()+
                '</table>';                                                         

        title = "Appointment reports from: "+
                $("#search_fromdate").val()+
                " to: "+$("#search_todate").val();

        var head = "<title>" +                                         
                    title + 
                    "</title>" +
                    "<link href='../css/header.css' rel='stylesheet' />" +
                    "<style type='text/css'>" +
                    "*{margin: 0;padding: 0;} \n\
                    b{font-family: Arial,Helvetica,sans-serif; font-size: 12px;} \n\
                    body{color: #181818; font-family: 'Exo',Helvetica,Arial,sans-serif; font-size: 12px; padding:5px;} \n\
                    th{font-size: 11px;color: #000;font-weight: bold;background-color: #ccc;text-align:left;} \n\
                    td.altRow{background: #f7f7f7 !important; font-size: 11px;padding-left: 3px;} \n\
                    table{border:none !important;} img{display:none;} \n\
                    td{font-size: 14px;padding-left: 10px;padding-right: 5px;}\n\
                    tr td{border-bottom: 1px solid;padding-top: 4px;}\n\
                    tr th{border-bottom: 1px solid;}\n\
                    td a{color: #000;text-decoration: none;}" +
                    "</style>";

        content = "<html><head>" + 
                    head +
                    "</head>" +
                    "<body>" +
                    body +
                    "</body></html>";

        var docprint=window.open("","",disp_setting); 

        docprint.document.open(); 
        docprint.document.write(content); 
        docprint.document.close(); 
        docprint.focus(); 
        docprint.print(); 
        docprint.close();
    }
    
    //drawAppointmentsTable();
    //drawAppointmentsTableMultiple();
});

function drawAllAppointmentsTable() {
    var searchquery1 = document.getElementById('searchquery').value.trim();
    var searchquery2 = document.getElementById('searchquery2').value.trim();
    var searchquery3 = document.getElementById('searchquery3').value.trim();

    var params = {"searchquery1": searchquery1, "searchquery2": searchquery2, "searchquery3": searchquery3};
    var searchData = params;

    if ($.fn.dataTable.isDataTable('#all table')) {
        $('#all table').DataTable({
            "destroy": true,
            "processing": true,
            "serverSide": false,                            
            "ajax": {
                url: hostName+"/api/apmnt/globalsearchbyqueryAll",
                type: "POST",
                async: false,
                dataType: "json",
                data: searchData,                    
                headers:{
                    "Content-Type":"application/json",
                    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                    "Authorization": bauth,
                }
            },                          
            "columns": [
                { "data": "gp_id" },
                { "data": "gp_visitor_accomp_persons" },
                { "data": "gp_visitor_name" },
                { "data": "gp_visitor_vehicle_no" }                    
            ],
            "bFilter" : false,     
            "columnDefs": [
                                { "orderable": false, "targets": [] }
                            ],
                            "order": [[ 0, 'desc' ]],           
            "iDisplayLength": 25,
            "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, 'All']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).attr("id", aData['gp_id']);                    
                $('td', nRow).contents().wrap('<a onclick="fetchidinfo(this)" data-toggle="modal" href="#popup-basic" />');
                return nRow;
            }
        });
    } else {
        $('#all table').DataTable({
            "processing": true,
            "serverSide": false,                            
            "ajax": {
                url: hostName+"/api/apmnt/globalsearchbyqueryAll",
                type: "POST",
                async: false,
                dataType: "json",
                data: searchData,                    
                headers:{
                    "Content-Type":"application/json",
                    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                    "Authorization": bauth,
                }
            },                            
            "columns": [
                { "data": "gp_id" },
                { "data": "gp_visitor_accomp_persons" },
                { "data": "gp_visitor_name" },
                { "data": "gp_visitor_vehicle_no" }                    
            ],
            "bFilter" : false, 
            "columnDefs": [
                                { "orderable": false, "targets": [] }
                            ],
                            "order": [[ 0, 'desc' ]],               
            "iDisplayLength": 25,
            "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, 'All']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).attr("id", aData['gp_id']);
                $('td', nRow).contents().wrap('<a onclick="fetchidinfo(this)" data-toggle="modal" href="#popup-basic" />');
                return nRow;
            }                                   
        });
    }
}

function drawAppointmentsTable() {
    var searchquery1 = document.getElementById('searchquery').value.trim();
    var searchquery2 = document.getElementById('searchquery2').value.trim();
    var searchquery3 = document.getElementById('searchquery3').value.trim();

    var params = {"searchquery1": searchquery1, "searchquery2": searchquery2, "searchquery3": searchquery3};
    var searchData = params;

    if ($.fn.dataTable.isDataTable('#single table')) {
        $('#single table').DataTable({
            "destroy": true,
            "processing": true,
            "serverSide": false,                            
            "ajax": {
                url: hostName+"/api/apmnt/globalsearchbyquery",
                type: "POST",
                async: false,
                dataType: "json",
                data: searchData,                    
                headers:{
                    "Content-Type":"application/json",
                    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                    "Authorization": bauth,
                }
            },                          
            "columns": [
                { "data": "gp_id" },
                { "data": "gp_visitor_accomp_persons" },
                { "data": "gp_visitor_name" },
                { "data": "gp_visitor_vehicle_no" }                    
            ],
            "bFilter" : false,     
            "columnDefs": [
                                { "orderable": false, "targets": [] }
                            ],
                            "order": [[ 0, 'desc' ]],           
            "iDisplayLength": 25,
            "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, 'All']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).attr("id", aData['gp_id']);                    
                $('td', nRow).contents().wrap('<a onclick="fetchidinfo(this)" data-toggle="modal" href="#popup-basic" />');
                return nRow;
            }
        });
    } else {
        $('#single table').DataTable({
            "processing": true,
            "serverSide": false,                            
            "ajax": {
                url: hostName+"/api/apmnt/globalsearchbyquery",
                type: "POST",
                async: false,
                dataType: "json",
                data: searchData,                    
                headers:{
                    "Content-Type":"application/json",
                    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                    "Authorization": bauth,
                }
            },                            
            "columns": [
                { "data": "gp_id" },
                { "data": "gp_visitor_accomp_persons" },
                { "data": "gp_visitor_name" },
                { "data": "gp_visitor_vehicle_no" }                    
            ],
            "bFilter" : false, 
            "columnDefs": [
                                { "orderable": false, "targets": [] }
                            ],
                            "order": [[ 0, 'desc' ]],               
            "iDisplayLength": 25,
            "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, 'All']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).attr("id", aData['gp_id']);
                $('td', nRow).contents().wrap('<a onclick="fetchidinfo(this)" data-toggle="modal" href="#popup-basic" />');
                return nRow;
            }                                   
        });
    }
}

function drawAppointmentsTableMultiple() {
    var searchquery1 = document.getElementById('searchquery').value.trim();
    var searchquery2 = document.getElementById('searchquery2').value.trim();
    var searchquery3 = document.getElementById('searchquery3').value.trim();

    var params = {"searchquery1": searchquery1, "searchquery2": searchquery2, "searchquery3": searchquery3};
    var searchData = params;

    if ($.fn.dataTable.isDataTable('#multiple table')) {
        $('#multiple table').DataTable({
            "destroy": true,
            "processing": true,
            "serverSide": false,                            
            "ajax": {
                url: hostName+"/api/apmnt/globalsearchbyqueryMultiple",
                type: "POST",
                async: false,
                dataType: "json",
                data: searchData,                    
                headers:{
                    "Content-Type":"application/json",
                    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                    "Authorization": bauth,
                }
            },                          
            "columns": [
                { "data": "gp_id" },
                { "data": "gp_visitor_accomp_persons" },
                { "data": "gp_visitor_name" },
                { "data": "gp_visitor_vehicle_no" },
                { "data": "gp_gkeep_dispatch_count" } ,
                { "data": "gp_apmnt_last_visit" }                     
            ],
            "bFilter" : false,     
            "columnDefs": [
                                { "orderable": false, "targets": [] }
                            ],
                            "order": [[ 0, 'desc' ]],           
            "iDisplayLength": 25,
            "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, 'All']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).attr("id", aData['gp_id']);                    
                $('td', nRow).contents().wrap('<a onclick="fetchidinfo(this)" data-toggle="modal" href="#popup-basic" />');
                return nRow;
            }
        });
    } else {
        $('#multiple table').DataTable({
            "processing": true,
            "serverSide": false,                            
            "ajax": {
                url: hostName+"/api/apmnt/globalsearchbyqueryMultiple",
                type: "POST",
                async: false,
                dataType: "json",
                data: searchData,                    
                headers:{
                    "Content-Type":"application/json",
                    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                    "Authorization": bauth,
                }
            },                            
            "columns": [
                { "data": "gp_id" },
                { "data": "gp_visitor_accomp_persons" },
                { "data": "gp_visitor_name" },
                { "data": "gp_visitor_vehicle_no" },
                { "data": "gp_gkeep_dispatch_count" } ,
                { "data": "gp_apmnt_last_visit" } 
            ],
            "bFilter" : false, 
            "columnDefs": [
                                { "orderable": false, "targets": [] }
                            ],
                            "order": [[ 0, 'desc' ]],               
            "iDisplayLength": 25,
            "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, 'All']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).attr("id", aData['gp_id']);
                $('td', nRow).contents().wrap('<a onclick="fetchidinfo(this)" data-toggle="modal" href="#popup-basic" />');
                return nRow;
            }                                   
        });
    }
}