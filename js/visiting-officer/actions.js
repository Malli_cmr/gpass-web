function newRequest(){
  
  $('#newreqfrom').removeClass('showitem');
  
  $('#myaccount').addClass('showitem');   
 
  $('#pageheader .titlespan').text('New Request');
   
  $("#errmsg").text(''); 
}

$(document).ready(function(){
    $.ajax({
        type: "GET",
        url:hostName+"/api/officerinfo",
        async: false,
        dataType: "json",
        headers:{
            "Content-Type":"application/json",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
            "Authorization": bauth,
        },
        success: function (data){
            if(data != null){                        
                var select = document.getElementById("officer");                
               
                var result = data.data;	
                var option;  
                
                for(var i=0; i < result.length; i++) {
                    option = document.createElement("option");
                    option.value = result[i].gp_usr_id;
                    option.text = result[i].gp_usr_emp_name;
                    select.appendChild(option);
                }                
           }
       }
   });
});

$(".right-nav ul li").on("click tap", function(){
	$(".right-nav ul li.active").removeClass('active');
	$(this).addClass('active');	
});

function myaccount() {
	var user = JSON.parse(sessionStorage.getItem('user'));
	$("#reloadscript").remove();
	$("#errmsg").text('');
	$('#myac_gp_usr_emp_name').val(user.gp_usr_emp_name);
    $('#myac_gp_usr_emp_emailid').val(user.gp_usr_emp_emailid);
    $('#myac_gp_usr_emp_mobileno').val(user.gp_usr_emp_mobileno);
    $('#myac_gp_usr_emp_passwd').val('');
    $('#myac_gp_usr_emp_deptmnt').val(user.gp_usr_emp_deptmnt);
    $('#myac_gp_usr_emp_designation').val(user.gp_usr_emp_designation);
    $('#myac_gp_usr_emp_addtional_info').val(user.gp_usr_emp_addtional_info);
    		
	$('#newreqfrom').addClass('showitem');
	
  	$('#myaccount').removeClass('showitem');  
     
  	$('#pageheader .titlespan').text('My Profile'); 
  	
}

var pid;

function fetchidinfo (sender) {
	$("#errmsg").text('');
    var t = sender.parentNode.parentNode;
    pid = t.getAttribute('id'); 

  $.ajax({
      type: "GET",
      url:hostName+"/api/apmnt/"+pid,
      async: false,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var apmntdata = succdata.data;
         var entrytype ; 
          if(apmntdata.gp_visitor_apmnt_options==1){
            entrytype ="Single";
          }else if(apmntdata.gp_visitor_apmnt_options==2){
            entrytype ="Multiple";
          }
         var noofvehicle = apmntdata.gp_visitor_vehicle_no;
         var vehiclenos = apmntdata.gp_visitor_vehicle_no;
         vehiclenos = vehiclenos.substr(vehiclenos.indexOf(",") + 1);
            noofvehicle = noofvehicle.split(",");
       var noofppl = apmntdata.gp_visitor_accomp_persons;
       var pplnames = apmntdata.gp_visitor_accomp_persons;
         pplnames = pplnames.substr(pplnames.indexOf(",") + 1);
          noofppl = noofppl.split(",");
          var fnoppl = Number(noofppl[0]) ;
        $('#apmntdetailinfo').empty();
        
    $('#apmntdetailinfo').append('<tr><td>Visiting Id </td><td id="#apmntidval">'+apmntdata.gp_id+'</td></tr><tr><td>Entry Type</td><td>'+entrytype+'</td></tr><tr><td>Entry From Time</td><td>'+apmntdata.gp_tomeet_from+'</td></tr><tr><td>Entry To Time</td><td>'+apmntdata.gp_tomeet_to+'</td></tr><tr><td>Visiting Officer Name</td><td>'+apmntdata.gp_tomeet_name+'</td></tr><tr><td>Visitor Name</td><td>'+apmntdata.gp_visitor_name+'</td></tr><tr><td>Visitor Email id</td><td>'+apmntdata.gp_visitor_email+'</td></tr><tr><td>Visitor Mobile No</td><td>'+apmntdata.gp_visitor_mobile_no+'</td></tr><tr><td>Visitor ID Proof</td><td>'+apmntdata.gp_visitor_idproof+'</td></tr><tr><td>Visitor Address</td><td>'+apmntdata.gp_visitor_address+'</td></tr><tr><td>No of Vehicles</td><td><input type="text" id="'+apmntdata.gp_id+'novehicle"value="'+noofvehicle[0]+'"</td></tr><tr><td>Visitor Vehicle No(s)</td><td><input type="text" id="'+apmntdata.gp_id+'vehiclenos"value="'+vehiclenos+'"</td></tr><tr><td>No of Accompanying persons </td><td><input type="text" id="'+apmntdata.gp_id+'noppl"value="'+fnoppl+'"></td></tr><tr><td>Visitor Accompanying persons</td><td><input type="text" id="'+apmntdata.gp_id+'names"value="'+pplnames+'" ></td></tr></td></tr><tr><td>Purpose of Visit</td><td>'+apmntdata.gp_visitor_notes+'</td></tr><tr><td>Notes to Security Officer</td><td> <input type="text" id="'+apmntdata.gp_id+'ofcnotes"value="'+apmntdata.gp_officer_approval_notes+'"></td></tr>');
            }}});
 
  
  
}

var inserted = null;

function dataURItoBlob(dataURI) {
	if(!dataURI) return null;
	else var mime = dataURI.match(/^data\:(.+?)\;/);
	var byteString = atob(dataURI.split(',')[1]);
	var ab = new ArrayBuffer(byteString.length);
	var ia = new Uint8Array(ab);
	for (var i = 0; i < byteString.length; i++) {
		ia[i] = byteString.charCodeAt(i);
	}
	return new Blob([ab], {type: mime[1]});
}

$(document).ready(function(){
	$(".right-nav ul li").removeClass('active');
	$(".right-nav ul li:first-child").addClass('active');	
	
	$(".right-nav ul li a").on("click tap", function(){
		$(".right-nav ul li").removeClass('active');
		$(this).parent().addClass('active');	
	});	
	
	$("#newreq_form").on("submit", function(){
		var thisFormData = new FormData(this);
		thisFormData.append("app_id", inserted.gp_id);
		
		inputname = "image";
		inputblob = dataURItoBlob(thisImage);
		filename = inputblob.type.replace("/", ".");						
		thisFormData.append(inputname, inputblob, filename);
		
		$.ajax({
			type: "POST",
			url:hostName+"/api/apmnt/upload",			
			data : thisFormData,	
			async: false,
                        dataType: "json",			
			processData: false,
			contentType: false,
			headers:{				
				"Access-Control-Allow-Origin": "*",
				"Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, OPTIONS",
				"Authorization": bauth,
			},
			success: function (data){				
				if(data.success == "true") {
					$('#apmntdetailinfo').empty();	
                                        
                                        var temphtml = '<tr><td>Entry From Time</td><td>'+inserted.gp_tomeet_from+'</td><td>Entry To Time</td><td>'+inserted.gp_tomeet_to+'</td></tr>'+
                                        '<tr><td width="36%">Visiting Id </td><td width="30%">'+inserted.gp_id+'</td><td width="34%">Visitor Photo</td><td rowspan="4"><img src="'+hostName+'/api/appimgs/'+data.filename+'" width="150"></td></tr>'+
                                        '<tr><td>Visiting Officer Name</td><td colspan="2">'+inserted.gp_tomeet_name+'</td></tr>'+
                                        '<tr><td>Visitor Name</td><td colspan="3">'+inserted.gp_visitor_name+'</td></tr>'+
                                        '<tr><td>Visitor Email id</td><td colspan="3">'+inserted.gp_visitor_email+'</td></tr>'+
                                        '<tr><td>Visitor Mobile No</td><td>'+inserted.gp_visitor_mobile_no+'</td><td>Visitor ID Proof</td><td>'+inserted.gp_visitor_idproof+'</td></tr>'+
                                        '<tr><td>Visitor Address</td><td colspan="3">'+inserted.gp_visitor_address+'</td></tr>'+
                                        '<tr><td>Purpose of Visit</td><td>'+inserted.gp_visitor_notes+'</td><td>Notes to Security Officer</td><td>'+inserted.gp_officer_approval_notes+'</td></tr>'+
                                        '<tr><td colspan="4"></td></tr>';
                                        $('#apmntdetailinfo').append(temphtml);
                                        
					/*$('#apmntdetailinfo').append('<tr><td>Entry From Time </td><td>'+inserted.gp_id+'</td></tr>'+					
					'<tr><td>Entry From Time</td><td>'+inserted.gp_tomeet_from+'</td></tr>'+
					'<tr><td>Entry To Time</td><td>'+inserted.gp_tomeet_to+'</td></tr>'+
					'<tr><td>Visiting Officer Name</td><td>'+inserted.gp_tomeet_name+'</td></tr>'+
					'<tr><td>Visitor Name</td><td>'+inserted.gp_visitor_name+'</td></tr>'+
					'<tr><td>Visitor Email id</td><td>'+inserted.gp_visitor_email+'</td></tr>'+
					'<tr><td>Visitor Mobile No</td><td>'+inserted.gp_visitor_mobile_no+'</td></tr>'+
					'<tr><td>Visitor ID Proof</td><td>'+inserted.gp_visitor_idproof+'</td></tr>'+
					'<tr><td>Visitor Address</td><td>'+inserted.gp_visitor_address+'</td></tr>'+
					'<tr><td>Purpose of Visit</td><td>'+inserted.gp_visitor_notes+'</td></tr>'+
					'<tr><td>Notes to Security Officer</td><td>'+inserted.gp_officer_approval_notes+'</td></tr>'+
					'<tr><td>Visitor Photo</td><td><img src="'+hostName+'/api/appimgs/'+data.filename+'" width="275" /></td></tr>');*/
                                
					$('#popup-basic').modal('show');
				}
			}
		});
						
		return false;
	});
});

function printApp() { 

	var disp_setting="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
	disp_setting+="scrollbars=yes,width=600, height=700, left=100, top=25"; 

	var body, head, content, title;                            

	/*body = '<table width="100%" cellpadding="0" cellspacing="0">'+			
			$("table#appInfoTable > tbody").html()+
			'</table>'; */
    
        body = $("#popup-basic").html(); 

	var head = "<title>" +                                         
				title + 
				"</title>" +
                                "<link href='../css/bootstrap.min.css' rel='stylesheet' />" +
				"<link href='../css/header.css' rel='stylesheet' />" +
                                "<link href='../css/custom.css' rel='stylesheet' />" +
				"<style type='text/css'>" +
				"*{margin: 0;padding: 0;} \n\
				b{font-family: Arial,Helvetica,sans-serif; font-size: 12px;} \n\
				body{background: #fff !important; color: #181818; font-family: 'Exo',Helvetica,Arial,sans-serif; font-size: 12px; padding:5px;} \n\
				table{border:none !important;} img{display:none;} \n\
				td{font-size: 14px;padding-left: 10px;padding-right: 5px;}\n\
				button{display: none !important;}\n\
                                tr td{padding-top: 12px;}\n\
				tr td img{display: block;}\n\ " +
				"</style>";

	content = "<html><head>" + 
				head +
				"</head>" +
				"<body>" +
				body +
				"</body></html>";

	var docprint=window.open("","",disp_setting); 

	docprint.document.open(); 
	docprint.document.write(content); 
	docprint.document.close(); 
	docprint.focus(); 
	docprint.print(); 
	//docprint.close();
}

function apmntreqsubmit(){
    $("#errmsg").text('');
   
    $(".errline").removeClass('errline');
    
    if(document.getElementById("fullName").value.length==0)
    {        
        $('#errmsg').empty();
        $('#errmsg').text('Please enter Fullname');
        
        $("#fullName").addClass('errline');
        window.location.href="#";        
        
    }else if(document.getElementById("phoneNumber").value.length<10)
    {
       $('#errmsg').empty();
        $('#errmsg').text('Please enter valid phone number');
        
         $("#phoneNumber").addClass('errline');
         window.location.href="#";
    }else if(document.getElementById("email").value!="" && !emailre.test(document.getElementById("email").value))
    {
        $('#errmsg').empty();
        $('#errmsg').text('Please enter valid Email');
        
        $("#email").addClass('errline');
        window.location.href="#";
         
    }else if(document.getElementById("purpose").value =='' )
    {
      $('#errmsg').empty();
      $('#errmsg').text('Please select purpose of visit');
      
        $("#purpose").addClass('errline');
         window.location.href="#";
        
    }else if(document.getElementById("officer").value =='' )
    {
      $('#errmsg').empty();
      $('#errmsg').text('Please select whoom to meet');
      
        $("#purpose").addClass('errline');
         window.location.href="#";
        
    }else 
    {  
        $.ajax({
            type: "GET",
            url:hostName+"/api/userinfo/"+$("#officer").val(),
            async: false,
            dataType: "json",
            headers:{
                "Content-Type":"application/json",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
            success: function (data){
                if(data != null){
                    var succdata = data;
                    var userfobj = succdata.data;
                    
                    var params;
                    var registerdata;
                    var enttype = 1;

                    var cdt = new Date();    
                    var ffrom = cdt.getFullYear()+'-'+(cdt.getMonth()+1)+'-'+cdt.getDate()+' '+cdt.getHours()+':'+cdt.getMinutes()+':'+cdt.getSeconds();        
                    var fto = cdt.getFullYear()+'-'+(cdt.getMonth()+1)+'-'+cdt.getDate()+' '+(cdt.getHours()+4)+':'+cdt.getMinutes()+':'+cdt.getSeconds(); 

                    params = {                 
                      "gp_visiting_id": "",
                      "gp_tomeet_name":userfobj.gp_usr_emp_name,
                      "gp_tomeet_email": userfobj.gp_usr_emp_emailid,
                      "gp_tomeet_mobile_no": userfobj.gp_usr_emp_mobileno,
                      "gp_tomeet_from": ffrom,
                      "gp_tomeet_to": fto,
                      "gp_visitor_name": document.getElementById("fullName").value,
                      "gp_visitor_email": document.getElementById("email").value,
                      "gp_visitor_mobile_no": document.getElementById("phoneNumber").value,
                      "gp_visitor_vehicle_no": "",
                      "gp_visitor_accomp_persons": "",
                      "gp_visitor_notes": document.getElementById("purpose").value,
                      "gp_visitor_photo_url": "vistorphotourl",
                      "gp_visitor_apmnt_options": enttype,
                      "gp_officer_approval_flag": "1",
                      "gp_officer_approval_notes": "visitingoffappnotes",
                      "gp_security_approval_flag": "0",
                      "gp_security_approval_notes": "visitingappnotes",
                      "gp_gkeep_dispatch_count": "0",
                      "gp_apmnt_last_visit": "",
                      "gp_user_id":userfobj.gp_usr_id,
                      "gp_visitor_idproof": document.getElementById("gp_visitor_idproof").value,
                      "gp_visitor_address": document.getElementById("gp_visitor_address").value
                    };

                    registerdata = JSON.stringify(params);							
										
                    $.ajax({
                        type: "POST",
                        url:hostName+"/api/apmnt",
                        async: false,
                        data : registerdata,
                        dataType: "json",
                        headers:{
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Origin": "*",
                            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS,FILES",
                            "Authorization": bauth,
                        },
                        success: function (data){
                            if(data != null){
                                var succdata = data;
                                var alertdata = succdata.data;
								inserted = alertdata;
                                var msg = "Your Gate Pass request submitted successfully and please note the following gate pass id "+alertdata.gp_id+" for your reference";

                                $('#fullName').val('');
                                $('#phoneNumber').val('');
                                $('#email').val('');
                                $('#officer').val('');
                                $('#purpose').val('');
                                $('#pic').val('');
                                $('#purposeNotes').val('');
                                $('#gp_visitor_idproof').val('');
                                $('#gp_visitor_address').val('');
                                
                                $("#newreq_form").submit();
                            }
                        }
                    });
                }
            }
        });    
    }
  
};