$(".right-nav ul li").on("click tap", function(){
	$(".right-nav ul li.active").removeClass('active');
	$(this).addClass('active');	
});

function newRequest(){
  
  $('#newreqfrom').removeClass('showitem');
  $('#apmntsall').addClass('showitem');
  
  
}
function pendingapprovals(){
  
  $('#newreqfrom').addClass('showitem');
  function getallapmnts(){
    var userobject = sessionStorage.getItem('user');
      var userfobj = JSON.parse(userobject);
      
    $.ajax({
      type: "GET",
      url:hostName+"/api/apmnt/offtoapprove/"+userfobj.gp_usr_id,
      async: false,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var apmntdata = succdata.data;
          $('#apmntlist').empty();
          $('#apmntlist2').empty();
         $.each(apmntdata, function(i, reqname){
           
         if(reqname.gp_officer_approval_flag == 0){
         
         $('#apmntlist').append('<tr id="'+reqname.gp_id+'"><td  >'+reqname.gp_id+'</td><td>'+reqname.gp_visitor_name+'</td><td>'+reqname.gp_tomeet_from+'</td><td id="'+reqname.gp_id+'"><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">details</a></td></tr>'); 
         }else if(reqname.gp_officer_approval_flag == 2){
           
           $('#apmntlist2').append('<tr id="'+reqname.gp_id+'"><td  >'+reqname.gp_id+'</td><td>'+reqname.gp_visitor_name+'</td><td>'+reqname.gp_tomeet_from+'</td><td id="'+reqname.gp_id+'"><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">details</a></td></tr>');
         }
        
            });
                  
        }
      }
    
  
  });
  
  
    
  };
  
  getallapmnts();
  $('#apmntsall').removeClass('showitem');
  
}

var pid;

function fetchidinfo (sender) {
    var t = sender.parentNode;
    pid = t.getAttribute('id'); 

  $.ajax({
      type: "GET",
      url:hostName+"/api/apmnt/"+pid,
      async: false,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var apmntdata = succdata.data;
         var entrytype ; 
          if(apmntdata.gp_visitor_apmnt_options==1){
            entrytype ="Single";
          }else if(apmntdata.gp_visitor_apmnt_options==2){
            entrytype ="Multiple";
          }
         var noofvehicle = apmntdata.gp_visitor_vehicle_no;
         var vehiclenos = apmntdata.gp_visitor_vehicle_no;
         vehiclenos = vehiclenos.substr(vehiclenos.indexOf(",") + 1);
            noofvehicle = noofvehicle.split(",");
       var noofppl = apmntdata.gp_visitor_accomp_persons;
       var pplnames = apmntdata.gp_visitor_accomp_persons;
         pplnames = pplnames.substr(pplnames.indexOf(",") + 1);
          noofppl = noofppl.split(",");
        $('#apmntdetailinfo').empty();
        
        $('#apmntdetailinfo').append('<tr><td>Visiting Id </td><td id="#apmntidval">'+apmntdata.gp_id+'</td></tr><tr><td>Entry Type</td><td>'+entrytype+'</td></tr><tr><td>Entry From Time</td><td>'+apmntdata.gp_tomeet_from+'</td></tr><tr><td>Entry To Time</td><td>'+apmntdata.gp_tomeet_to+'</td></tr><tr><td>Visiting Officer Name</td><td>'+apmntdata.gp_tomeet_name+'</td></tr><tr><td>Visitor Name</td><td>'+apmntdata.gp_visitor_name+'</td></tr><tr><td>Visitor Email id</td><td>'+apmntdata.gp_visitor_email+'</td></tr><tr><td>Visitor Mobile No</td><td>'+apmntdata.gp_visitor_mobile_no+'</td></tr><tr><td>No of Vehicles</td><td>'+noofvehicle[0]+'</td></tr><tr><td>Visitor Vehicle No</td><td>'+vehiclenos+'</td></tr><tr><td>No of persons Visiting</td><td>'+noofppl[0]+'</td></tr><tr><td>Visitor Accompanying persons</td><td><input type="text" id="'+apmntdata.gp_id+'names"value="'+pplnames+'" ></td></tr></td></tr><tr><td>Purpose of Visit</td><td>'+apmntdata.gp_visitor_notes+'</td></tr><tr><td>Notes to Security Officer</td><td> <input type="text" id="'+apmntdata.gp_id+'ofcnotes"value="'+apmntdata.gp_officer_approval_notes+'"></td></tr>');

            }}});
 
  
  
}

function declinebyofc(){
  
  var params;
     var registerdata;
     var enttype = $( '#etype' ).val();
     var fromdate = $('#fromdate').val();
     var fromtime = $('#fromtime').val();
     var  ffrom = fromdate+" "+fromtime+":00" ;
     var todate = $('#todate').val();
     var totime = $('#totime').val();
     var  fto = todate+" "+totime+":00" ;  
     var ofcnotes = pid+"ofcnotes";
   params = {
                  "gp_officer_approval_flag": "2",
                  "gp_officer_approval_notes": document.getElementById(ofcnotes).value,
                  "gp_security_approval_flag": "0",
                  "gp_security_approval_notes": "secappnotes",
                  "gp_gkeep_dispatch_count": 0,
                  "gp_apmnt_last_visit": "2014-12-20 06:16:14"
                  
                };

  registerdata = JSON.stringify(params);
  var apmntidval = $('#apmntidval').text();
  
  $.ajax({
      type: "PUT",
      url:hostName+"/api/apmnt/updateofcflag/"+pid,
      async: false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var alertdata = succdata.data;
         var msg = "Your Gate Pass request ignored   "+alertdata.gp_id;
        
         
         alert(msg);
         pendingapprovals();
         $('#popup-basic').modal('hide');
                  
        }
      },
      });
   

  
  
}


function approvebyofc(){
  
  var params;
     var registerdata;
     var enttype = $( '#etype' ).val();
     var fromdate = $('#fromdate').val();
     var fromtime = $('#fromtime').val();
     var  ffrom = fromdate+" "+fromtime+":00" ;
     var todate = $('#todate').val();
     var totime = $('#totime').val();
     var  fto = todate+" "+totime+":00" ;  
    var ofcnotes = pid+"ofcnotes";
   params = {
                  "gp_officer_approval_flag": "1",        
                  "gp_officer_approval_notes":document.getElementById(ofcnotes).value,
                  "gp_security_approval_flag": "0",
                  "gp_security_approval_notes": "secappnotes",
                  "gp_gkeep_dispatch_count": 0,
                  "gp_apmnt_last_visit": "2014-12-20 06:16:14"
                  
                };

  registerdata = JSON.stringify(params);
  var apmntidval = $('#apmntidval').text();
  
  $.ajax({
      type: "PUT",
      url:hostName+"/api/apmnt/updateofcflag/"+pid,
      async: false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var alertdata = succdata.data;
         var msg = "Your Gate Pass request forwarded successfully and please note the following gate pass id "+alertdata.gp_visiting_id+" for your reference";
         
         
         alert(msg);
         pendingapprovals();
         $('#popup-basic').modal('hide');
         
        }
      },
      });  
}

function reqtypesubmit(){
  console.log(document.getElementById("gpreq").value);
  if(document.getElementById("gpreq").value=0){
    
    alert('please select the request type');
  }else if(document.getElementById("gpreq").value=1){
     $('#smsreq').removeClass('showitem');
      $('#emailreq').addClass('showitem');
      $('#selfform').addClass('showitem');
    
  }else if(document.getElementById("gpreq").value=2){
    $('#selfform').addClass('showitem');
    $('#smsreq').addClass('showitem');
      $('#emailreq').removeClass('showitem');
  }else if(document.getElementById("gpreq").value=3){
    
    $('#smsreq').addClass('showitem');
      $('#emailreq').addClass('showitem');
      $('#selfform').removeClass('showitem');
  }
  
  
}

function apmntreqsubmit(){
    
    var userobject = sessionStorage.getItem('user');
      var userfobj = JSON.parse(userobject);
      
   var vstr ="";
   var pstr ="";
   for(i=1; i<=document.getElementById("vehnos").value; i++){
          
      var str1 = "vehicalNumber"+i ;
      if(document.getElementById(str1).value.length ==0 ){
        alert("please enter all vehicle nos");
      }else{
     vstr = vstr +document.getElementById(str1).value+","; 
     }
   }
   for(i=1; i<=document.getElementById("personsnumber").value; i++){
      var str1 = "pnames"+i ;
     if(document.getElementById(str1).value.length ==0 ){
        alert("please enter all Persons names");
      }else{
     pstr = pstr +document.getElementById(str1).value+","; 
     }
     
   }
    
    
    $(".errline").removeClass('errline');
    
    if(document.getElementById("fullName").value.length==0)
    {
        
        $('#errmsg').empty();
        $('#errmsg').text('Please enter Fullname');
        
        $("#fullName").addClass('errline');
        window.location.href="#";
        
        
    }else if(document.getElementById("phoneNumber").value.length<10)
    {
       $('#errmsg').empty();
        $('#errmsg').text('Please enter Phone Number');
        
         $("#phoneNumber").addClass('errline');
         window.location.href="#";
        


    }else if(document.getElementById("email").value!="" && !emailre.test(document.getElementById("email").value))
    {
        $('#errmsg').empty();
        $('#errmsg').text('Please enter valid Email');
        
        $("#email").addClass('errline');
        window.location.href="#";
         
    }else if(document.getElementById("fromdate").value == ''  )
    {
       $('#errmsg').empty();
        $('#errmsg').text('Please enter From Date & Time');
        
        $("#fromdate").addClass('errline');
         window.location.href="#";
        
    }else if($( '#etype' ).val() == 2 && document.getElementById("todate").value == ''  )
    {
       $('#errmsg').empty();
        $('#errmsg').text('Please enter To Date & Time');
        
        $("#todate").addClass('errline');
         window.location.href="#";
        
    }else if($( '#etype' ).val() == 2 && document.getElementById("todate").value < document.getElementById("fromdate").value  )
    {
       $('#errmsg').empty();
        $('#errmsg').text('Please enter To time after  from time');
        
        $("#todate").addClass('errline');
         window.location.href="#";
        
    }else if(vstr.length==0 && document.getElementById("vehnos").value >0)
    {
      $('#errmsg').empty();
      $('#errmsg').text('Please enter vehicle Nos');
      
       $("#vehnos").addClass('errline');
         window.location.href="#";
        
    }else  if(pstr.length ==0 && document.getElementById("personsnumber").value >0)
    {
      $('#errmsg').empty();
      $('#errmsg').text('Please enter Person name(S) minimum of 5 characters');
      
      $("#personsnumber").addClass('errline');
         window.location.href="#";
        
    }else if(document.getElementById("purpose").value =='' )
    {
      $('#errmsg').empty();
      $('#errmsg').text('Please select purpose of visit');
      
        $("#purpose").addClass('errline');
         window.location.href="#";
        
    }else if(document.getElementById("gp_visitor_address").value.length <= 9 )
    {
      $('#errmsg').empty();
      $('#errmsg').text('Please enter address minimum of 10 characters');
         window.location.href="#";
        
    }else 
    {
      var params;
     var registerdata;
     var enttype = $( '#etype' ).val();
     var fromdate = $('#fromdate').val();
     var  ffrom = fromdate+":00" ;
     var totime = $('#todate').val();
     
     if(totime != '')
        var  fto = totime+":00" ; 
    else
        var  fto = "00-00-0000 00:00:00" ;  
 
     if(document.getElementById("vehnos").value > 0)
     var vehiclestr = document.getElementById("vehnos").value+','+vstr ;
     else
     var vehiclestr = '';
     
     if(document.getElementById("vehnos").value > 0)
     var peoplestr = document.getElementById("personsnumber").value+','+pstr;
     else
     var peoplestr = '';
 
   params = {
                 
                  "gp_visiting_id": "",
                  "gp_tomeet_name":userfobj.gp_usr_emp_name,
                  "gp_tomeet_email": userfobj.gp_usr_emp_emailid,
                  "gp_tomeet_mobile_no": userfobj.gp_usr_emp_mobileno,
                  "gp_tomeet_from": ffrom,
                  "gp_tomeet_to": fto,
                  "gp_visitor_name": document.getElementById("fullName").value,
                  "gp_visitor_email": document.getElementById("email").value,
                  "gp_visitor_mobile_no": document.getElementById("phoneNumber").value,
                  "gp_visitor_vehicle_no": vehiclestr,
                  "gp_visitor_accomp_persons": peoplestr,
                  "gp_visitor_notes": document.getElementById("purpose").value,
                  "gp_visitor_photo_url": "vistorphotourl",
                  "gp_visitor_apmnt_options": enttype,
                  "gp_officer_approval_flag": "0",
                  "gp_officer_approval_notes": "offappnotes",
                  "gp_security_approval_flag": "0",
                  "gp_security_approval_notes": "secappnotes",
                  "gp_gkeep_dispatch_count": "0",
                  "gp_apmnt_last_visit": "",
                  "gp_usr_id":userfobj.gp_usr_id,
                  "gp_visitor_idproof": document.getElementById("gp_visitor_idproof").value,
                  "gp_visitor_address": document.getElementById("gp_visitor_address").value
                };

  registerdata = JSON.stringify(params);
  
  $.ajax({
      type: "POST",
      url:hostName+"/api/apmnt",
      async: false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var alertdata = succdata.data;
         var msg = "Your Gate Pass request submitted successfully and please note the following gate pass id "+alertdata.gp_id+" for your reference";
         
           $('#modalmsg').empty();
          $('#modalmsg').text(msg);
          
          sessionStorage.setItem('message', msg);
          
          $('#fullName').val('');
           $('#phoneNumber').val('');
           $('#email').val('');
           $('#fromdate').val('');
           $('#todate').val('');
           $('#etype').val('');
           $('#vehnos').val('');
           $('#personsnumber').val('');
           $('#purpose').val('');
           $('#pic').val('');
           $('#purposeNotes').val('');
           $('#gp_visitor_idproof').val('');
           $('#gp_visitor_address').val('');
           
           for(i=1; i<=document.getElementById("vehnos").value; i++){          
			  	var str1 = "vehicalNumber"+i ;
			  	document.getElementById(str1).remove();
   			}
   			for(i=1; i<=document.getElementById("personsnumber").value; i++){
      			var str1 = "pnames"+i ;
     			document.getElementById(str1).remove();        
   			}
           
          $('#messagemodal').modal('show');
          window.open("success.html","_self");
          //pendingapprovals();
                  
        }
      },
      });
   
  }
  
};

function smsreqsubmit(){
  
    var userobject = sessionStorage.getItem('user');
      var userfobj = JSON.parse(userobject);
  
  if(document.getElementById("reqmobileno").value.length == 0){

            $('#modalmsg').empty();
          $('#modalmsg').text('Please Enter Mobile No');
           $('#messagemodal').modal('show');
  }else if(document.getElementById("reqmobileno").value.length != 10){
     $('#modalmsg').empty();
          $('#modalmsg').text('Please Enter valid 10 digit Mobile No');
           $('#messagemodal').modal('show');
     
  }else{
    var params;
     var registerdata;
     
    
   params = {
                 
  "mobileno":"91"+document.getElementById("reqmobileno").value,
  "formurl":hostName+"/enduser/index.html?id="+userfobj.gp_usr_id

                  
                };

  registerdata = JSON.stringify(params);
  
  $.ajax({
      type: "POST",
      url:hostName+"/api/apmnt/newreqsms",
      async: false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var alertdata = succdata.status;
      
         $('#modalmsg').empty();
          $('#modalmsg').text('Your Gate Pass request submitted successfully');
          $('#reqmobileno').val('');
           $('#messagemodal').modal('show');
            pendingapprovals();

         
                  
        }
      },
      });

    
  }
  
}

function emailreqsubmit(){
  
  var userobject = sessionStorage.getItem('user');
      var userfobj = JSON.parse(userobject);
      
  if(document.getElementById("reqemailid").value.length == 0){
    
        $('#modalmsg').empty();
         $('#modalmsg').text('Please Enter emailid');
        $('#messagemodal').modal('show');
         

    
  }else {
    var params;
     var registerdata;
     
    
   params = {
                 
  "email":document.getElementById("reqemailid").value,
  "formurl": hostName+"/enduser/index.html?id="+userfobj.gp_usr_id

                  
                };

  registerdata = JSON.stringify(params);
  
  $.ajax({
      type: "POST",
      url:hostName+"/api/apmnt/newreqmail",
      async: false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var alertdata = succdata.data;
       
         
         
    $('#modalmsg').empty();
         $('#modalmsg').text('Your Gate Pass request submitted successfully');
        $('#messagemodal').modal('show');
         pendingapprovals();
                  
        }
      },
      });

    
  }
  
}

