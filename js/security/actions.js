$(document).ready(function(){
	$(".right-nav ul li").on("click tap", function(){
		$(".right-nav ul li.active").removeClass('active');
		$(this).addClass('active');	
	});

	$("#approvals").on("click tap", function(){		
		pendingapprovals();
	});
			
	$("#myprofile").on("click tap", function(){		
		myaccount();
	});
	
	$("#logout").on("click tap", function(){		
		logoutaction();
	});
});
var apmntdata;
function newRequest(){
  
  $('#newreqfrom').removeClass('showitem');
  $('#apmntsall').addClass('showitem');
  
  
}
function pendingapprovals(){
  
  $('#newreqfrom').addClass('showitem');
  $("#myaccount").addClass('showitem');
  $("#pageheader .title").text('Approvals');	
  function getallapmnts(){
   
   drawPendingAppointmentsTable();
    drawIgnoredAppointmentsTableMultiple();
    return false;
      
    $.ajax({
      type: "GET",
      url:hostName+"/api/apmnt/sectoapprove/1",
      async: true,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var apmntdata = succdata.data;
          $('#apmntlist').empty();
          $('#apmntlist2').empty();
         $.each(apmntdata, function(i, reqname){
           
            var pplnames = reqname.gp_visitor_accomp_persons;
         pplnames = pplnames.substr(pplnames.indexOf(",") + 1);
             var vehiclenos = reqname.gp_visitor_vehicle_no;
         vehiclenos = vehiclenos.substr(vehiclenos.indexOf(",") + 1);
         if(reqname.gp_security_approval_flag == 0){
         
         $('#apmntlist').append('<tr id="'+reqname.gp_id+'"><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+reqname.gp_id+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+reqname.gp_visitor_name+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+pplnames+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+vehiclenos+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+reqname.gp_tomeet_from+'</a></td></tr>'); 
         }else if(reqname.gp_security_approval_flag == 2){
           
           $('#apmntlist2').append('<tr id="'+reqname.gp_id+'"><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+reqname.gp_id+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+reqname.gp_visitor_name+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+pplnames+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+vehiclenos+'</a></td><td><a href="#popup-basic"  data-toggle="modal" onclick="fetchidinfo(this)">'+reqname.gp_tomeet_from+'</a></td></tr>');
         }
        
            });
                  
        }
      }
    
  
  });
  
  
    
  };
  
  getallapmnts();
  $('#apmntsall').removeClass('showitem');
  
  window.setTimeout(function(){if(!$("#apmntsall").is(':hidden') && $(".modal").is(':hidden'))$("#appointments").trigger('tap');}, 30000);
  
}

var pid;

function fetchidinfo (sender) {
    var t = sender.parentNode.parentNode;
    pid = t.getAttribute('id'); 

  $.ajax({
      type: "GET",
      url:hostName+"/api/apmnt/"+pid,
      async: true,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var apmntdata = succdata.data;
         var entrytype ; 
          if(apmntdata.gp_visitor_apmnt_options==1){
            entrytype ="Single";
          }else if(apmntdata.gp_visitor_apmnt_options==2){
            entrytype ="Multiple";
          }
         var noofvehicle = apmntdata.gp_visitor_vehicle_no;
         var vehiclenos = apmntdata.gp_visitor_vehicle_no;
         vehiclenos = vehiclenos.substr(vehiclenos.indexOf(",") + 1);
            noofvehicle = noofvehicle.split(",");
       var noofppl = apmntdata.gp_visitor_accomp_persons;
       var pplnames = apmntdata.gp_visitor_accomp_persons;
         pplnames = pplnames.substr(pplnames.indexOf(",") + 1);
          noofppl = noofppl.split(",");
        $('#apmntdetailinfo').empty();
        
        $('#apmntdetailinfo').append('<tr><td>Visiting Id </td><td id="#apmntidval">'+apmntdata.gp_id+'</td></tr><tr><td>Entry Type</td><td>'+entrytype+'</td></tr><tr><td>Entry From Time</td><td>'+apmntdata.gp_tomeet_from+'</td></tr><tr><td>Entry To Time</td><td>'+apmntdata.gp_tomeet_to+'</td></tr><tr><td>Visiting Officer Name</td><td>'+apmntdata.gp_tomeet_name+'</td></tr><tr><td>Visitor Name</td><td>'+apmntdata.gp_visitor_name+'</td></tr><tr><td>Visitor Email id</td><td>'+apmntdata.gp_visitor_email+'</td></tr><tr><td>Visitor Mobile No</td><td>'+apmntdata.gp_visitor_mobile_no+'</td></tr><tr><td>Visitor ID Proof</td><td>'+apmntdata.gp_visitor_idproof+'</td></tr><tr><td>Visitor Address</td><td>'+apmntdata.gp_visitor_address+'</td></tr><tr><td>No of Vehicles</td><td>'+noofvehicle[0]+'</td></tr><tr><td>Visitor Vehicle No</td><td>'+vehiclenos+'</td></tr><tr><td>No of persons Visiting</td><td>'+noofppl[0]+'</td></tr><tr><td>Visitor Accompanying persons</td><td><input type="text" id="'+apmntdata.gp_id+'names"value="'+pplnames+'" ></td></tr></td></tr><tr><td>Purpose of Visit</td><td>'+apmntdata.gp_visitor_notes+'</td></tr><tr><td>Notes to Security Officer</td><td> <input type="text" id="'+apmntdata.gp_id+'ofcnotes"value="'+apmntdata.gp_officer_approval_notes+'"></td></tr><tr><td>Reason for ignorance(optional)</td><td> <input type="text" id="'+apmntdata.gp_id+'secnotes"value="'+apmntdata.gp_security_approval_notes+'"></td></tr>');

            }}});
 
  
  
}

function declinebyofc(sender){
  
  var params;
     var registerdata;
     var enttype = $( '#etype' ).val();
     var fromdate = $('#fromdate').val();
     var fromtime = $('#fromtime').val();
     var  ffrom = fromdate+" "+fromtime+":00" ;
     var todate = $('#todate').val();
     var totime = $('#totime').val();
     var  fto = todate+" "+totime+":00" ;  
      var ofcnotes = pid+"ofcnotes";
      var secnotes = pid+"secnotes";
   params = {
                  "gp_officer_approval_flag": "1",
                  "gp_officer_approval_notes": document.getElementById(ofcnotes).value,
                  "gp_security_approval_flag": "2",
                  "gp_security_approval_notes": document.getElementById(secnotes).value,
                  "gp_gkeep_dispatch_count": 0,
                  "gp_apmnt_last_visit": "2014-12-20 06:16:14"
                  
                };

  registerdata = JSON.stringify(params);
  var apmntidval = $('#apmntidval').text();
  
  $.ajax({
      type: "PUT",
      url:hostName+"/api/apmnt/updateofcflag/"+pid,
      async: true,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var alertdata = succdata.data;
         var msg = "Your Gate Pass request ignored successfully and please note the following gate pass id "+alertdata.gp_id+" for your reference";
         var subject = "AP Secretartiate Gate Pass Request Status";
         var ignoretext =" Your Gate Pass request with gate pass id "+alertdata.gp_id+" has been ignored because of reason "+alertdata.gp_security_approval_notes;
         var mailtext =" Hi <br /> Your Gate Pass request with gate pass id "+alertdata.gp_id+" has been ignored because of reason "+alertdata.gp_security_approval_notes+" <br /> Thanks, <br/> Security Team </br> AP Secretariate";
          pendingapprovals();
          $('#popup-basic').modal('hide');
          $('#loading').removeClass('showitem');
          var mobilenos = alertdata.gp_visitor_mobile_no+","+alertdata.gp_tomeet_mobile_no;
          //gpassnotification.sms(alertdata.gp_visitor_mobile_no, ignoretext);
          gpassnotification.sms(mobilenos, ignoretext);
          gpassnotification.email(alertdata.gp_tomeet_email, subject, mailtext);
          gpassnotification.email(alertdata.gp_visitor_email, subject, mailtext);
          $('#loading').text('Request Sent Successfully');
         $('#loading').addClass('showitem');
                  
        }
      },
      });
   

  
  
}


function approvebyofc(){
  
     var params;
     var alertdata;
     var registerdata;
     var subject;
     var mailtext;
     var ignoretext;
     var enttype = $( '#etype' ).val();
     var fromdate = $('#fromdate').val();
     var fromtime = $('#fromtime').val();
     var  ffrom = fromdate+" "+fromtime+":00" ;
     var todate = $('#todate').val();
     var totime = $('#totime').val();
     var  fto = todate+" "+totime+":00" ;  
     var ofcnotes = pid+"ofcnotes";
     var secnotes = pid+"secnotes";
   params = {
                  "gp_officer_approval_flag": "1",
                  "gp_officer_approval_notes": document.getElementById(ofcnotes).value,
                  "gp_security_approval_flag": "1",
                  "gp_security_approval_notes": document.getElementById(secnotes).value,
                  "gp_gkeep_dispatch_count": 0,
                  "gp_apmnt_last_visit": "2014-12-20 06:16:14"
                  
                };

  registerdata = JSON.stringify(params);
  var apmntidval = $('#apmntidval').text();
  
  $.ajax({
      type: "PUT",
      url:hostName+"/api/apmnt/updateofcflag/"+pid,
      async: false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
          alertdata = succdata.data;
         var msg = "Your Gate Pass request forwarded successfully and please note the following gate pass id "+alertdata.gp_visiting_id+" for your reference";
          subject = "AP Secretartiate Gate Pass Request Status";
          ignoretext =" Your Gate Pass request with gate pass id "+alertdata.gp_id+" has been Approved ";
          mailtext =" Hi <br /> Your Gate Pass request with gate pass id "+alertdata.gp_id+" has been approved <br /> Thanks, <br/> Security Team </br> AP Secretariate";
         
          $('#popup-basic').modal('hide');
          $('#loading').removeClass('showitem');
       
          $('#loading').text('Request Sent Successfully');
         $('#loading').addClass('showitem');
         
        }
      },
      });
    
    pendingapprovals();
     var mobilenos = alertdata.gp_visitor_mobile_no+",91"+alertdata.gp_tomeet_mobile_no;
          //gpassnotification.sms(alertdata.gp_visitor_mobile_no, ignoretext);
          //gpassnotification.sms(alertdata.gp_tomeet_mobile_no, ignoretext);
      gpassnotification.sms(mobilenos, ignoretext);
      gpassnotification.email(alertdata.gp_tomeet_email, subject, mailtext);
      gpassnotification.email(alertdata.gp_visitor_email, subject, mailtext);
  
}

function reqtypesubmit(){
  console.log(document.getElementById("gpreq").value);
  if(document.getElementById("gpreq").value=0){
    
    alert('please select the request type');
  }else if(document.getElementById("gpreq").value=1){
     $('#smsreq').removeClass('showitem');
      $('#emailreq').addClass('showitem');
      $('#selfform').addClass('showitem');
    
  }else if(document.getElementById("gpreq").value=2){
    $('#selfform').addClass('showitem');
    $('#smsreq').addClass('showitem');
      $('#emailreq').removeClass('showitem');
  }else if(document.getElementById("gpreq").value=3){
    
    $('#smsreq').addClass('showitem');
      $('#emailreq').addClass('showitem');
      $('#selfform').removeClass('showitem');
  }
  
  
}

function apmntreqsubmit(){
    
    var userobject = sessionStorage.getItem('user');
      var userfobj = JSON.parse(userobject);
    
    
    if(document.getElementById("fullName").value.length==0)
    {
        
        $('#errmsg').empty();
        $('#errmsg').text('Please enter Fullname');
        window.location.href="#";
        
        
    }else if(document.getElementById("email").value=="" || !emailre.test(document.getElementById("email").value))
    {
        $('#errmsg').empty();
        $('#errmsg').text('Please enter valid Email');
        window.location.href="#";
         
    }else if(document.getElementById("phoneNumber").value.length==0)
    {
       $('#errmsg').empty();
        $('#errmsg').text('Please enter Phone Number');
         window.location.href="#";
        


    }else if(document.getElementById("todate").value < document.getElementById("fromdate").value  )
    {
       $('#errmsg').empty();
        $('#errmsg').text('Please enter To time after  from time');
         window.location.href="#";
        


    }else if(document.getElementById("vehicalNumber").value.length==0 && document.getElementById("vehnos").value >0)
    {
      $('#errmsg').empty();
      $('#errmsg').text('Please enter vehicle Nos');
         window.location.href="#";
        
    }else  if(document.getElementById("pnames").value.length < 5 && document.getElementById("personsnumber").value >0)
    {
      $('#errmsg').empty();
      $('#errmsg').text('Please enter Person name(S) minimum of 5 characters');
         window.location.href="#";
        
    }else if(document.getElementById("purpose").value.length < 5 )
    {
      $('#errmsg').empty();
      $('#errmsg').text('Please enter Purpose minimum of 5 characters');
         window.location.href="#";
        
    }else 
    {
    $('#errmsg').text('');
      var params;
     var registerdata;
     var enttype = $( '#etype' ).val();
     var fromdate = $('#fromdate').val();
     var  ffrom = fromdate+":00" ;
     var totime = $('#todate').val();
     var  fto = totime+":00" ;  

     var vehiclestr = document.getElementById("vehnos").value+','+document.getElementById("vehicalNumber").value ;
     var peoplestr = document.getElementById("personsnumber").value+','+document.getElementById("pnames").value;
   params = {
                 
                  "gp_visiting_id": "",
                  "gp_tomeet_name":userfobj.gp_usr_emp_name,
                  "gp_tomeet_email": userfobj.gp_usr_emp_emailid,
                  "gp_tomeet_mobile_no": userfobj.gp_usr_emp_mobileno,
                  "gp_tomeet_from": ffrom,
                  "gp_tomeet_to": fto,
                  "gp_visitor_name": document.getElementById("fullName").value,
                  "gp_visitor_email": document.getElementById("email").value,
                  "gp_visitor_mobile_no": document.getElementById("phoneNumber").value,
                  "gp_visitor_vehicle_no": vehiclestr,
                  "gp_visitor_accomp_persons": peoplestr,
                  "gp_visitor_notes": document.getElementById("purpose").value,
                  "gp_visitor_photo_url": "vistorphotourl",
                  "gp_visitor_apmnt_options": enttype,
                  "gp_officer_approval_flag": "0",
                  "gp_officer_approval_notes": "offappnotes",
                  "gp_security_approval_flag": "0",
                  "gp_security_approval_notes": "secappnotes",
                  "gp_gkeep_dispatch_count": "0",
                  "gp_apmnt_last_visit": "",
                  "gp_user_id":userfobj.gp_usr_id
                };

  registerdata = JSON.stringify(params);
  
  $.ajax({
      type: "POST",
      url:hostName+"/api/apmnt",
      async: false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var alertdata = succdata.data;
         var msg = "Your Gate Pass request submitted successfully and please note the following gate pass id "+alertdata.gp_id+" for your reference";
         
           $('#modalmsg').empty();
          $('#modalmsg').text(msg);
          $('#messagemodal').modal('show');
          
          pendingapprovals();
          
           
         
                  
        }
      },
      });
   
  }
  
};

function myaccount(){		
	var user = JSON.parse(sessionStorage.getItem('user'));
	
	$('#myac_gp_usr_emp_name').val(user.gp_usr_emp_name);
    $('#myac_gp_usr_emp_emailid').val(user.gp_usr_emp_emailid);
    $('#myac_gp_usr_emp_mobileno').val(user.gp_usr_emp_mobileno);
    $('#myac_gp_usr_emp_passwd').val('');
    $('#myac_gp_usr_emp_deptmnt').val(user.gp_usr_emp_deptmnt);
    $('#myac_gp_usr_emp_designation').val(user.gp_usr_emp_designation);
    $('#myac_gp_usr_emp_addtional_info').val(user.gp_usr_emp_addtional_info);
    
    $("#pageheader .title").text('My Profile');
     		
	$("#myaccount").removeClass('showitem');
	$("#apmntsall").addClass('showitem');	
}

function smsreqsubmit(){
  
    var userobject = sessionStorage.getItem('user');
      var userfobj = JSON.parse(userobject);
  
  if(document.getElementById("reqmobileno").value.length == 0){

            $('#modalmsg').empty();
          $('#modalmsg').text('Please Enter Mobile No');
           $('#messagemodal').modal('show');
  }else if(document.getElementById("reqmobileno").value.length != 10){
     $('#modalmsg').empty();
          $('#modalmsg').text('Please Enter valid 10 digit Mobile No');
           $('#messagemodal').modal('show');
     
  }else{
    var params;
     var registerdata;
     
    
   params = {
                 
  "mobileno":"91"+document.getElementById("reqmobileno").value,
  "formurl":hostName+"/enduser/index.html?id="+userfobj.gp_usr_id

                  
                };

  registerdata = JSON.stringify(params);
  
  $.ajax({
      type: "POST",
      url:hostName+"/api/apmnt/newreqsms",
      async: false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var alertdata = succdata.status;
      
         $('#modalmsg').empty();
          $('#modalmsg').text('Your Gate Pass request submitted successfully');
          $('#reqmobileno').val('');
           $('#messagemodal').modal('show');
            pendingapprovals();

         
                  
        }
      },
      });

    
  }
  
}

function emailreqsubmit(){
  
  var userobject = sessionStorage.getItem('user');
      var userfobj = JSON.parse(userobject);
      
  if(document.getElementById("reqemailid").value.length == 0){
    
        $('#modalmsg').empty();
         $('#modalmsg').text('Please Enter emailid');
        $('#messagemodal').modal('show');
         

    
  }else {
    var params;
     var registerdata;
     
    
   params = {
                 
  "email":document.getElementById("reqemailid").value,
  "formurl": hostName+"/enduser/index.html?id="+userfobj.gp_usr_id

                  
                };

  registerdata = JSON.stringify(params);
  
  $.ajax({
      type: "POST",
      url:hostName+"/api/apmnt/newreqmail",
      async: false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var alertdata = succdata.data;
       
         
         
    $('#modalmsg').empty();
         $('#modalmsg').text('Your Gate Pass request submitted successfully');
        $('#messagemodal').modal('show');
         pendingapprovals();
                  
        }
      },
      });

    
  }
  
}

function drawPendingAppointmentsTable() {
    var userobject = sessionStorage.getItem('user');
    var userfobj = JSON.parse(userobject);
    
    if ($.fn.dataTable.isDataTable('#pendingAppointmentstab')) {
        $('#pendingAppointmentstab').DataTable({
            "destroy": true,
            "processing": true,
            "serverSide": false,                            
            "ajax": {
                url: hostName+"/api/apmnt/sectoapproveApproved/1",
                type: "GET",
                async: false,
                dataType: "json",                   
                headers:{
                    "Content-Type":"application/json",
                    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                    "Authorization": bauth,
                }
            },                          
            "columns": [
                { "data": "gp_id" },
                { "data": "gp_visitor_name" },
                { "data": "gp_visitor_accomp_persons" } ,
                { "data": "gp_visitor_vehicle_no" },  
                { "data": "gp_tomeet_from" }                  
            ],
            "bFilter" : false,     
            "columnDefs": [
                                { "orderable": false, "targets": [] }
                            ],
                            "order": [[ 0, 'desc' ]],           
            "iDisplayLength": 25,
            "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, 'All']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                var pplnames = $(nRow).find("td:eq(3)").html();                                
                $(nRow).find("td:eq(3)").html(pplnames.substr(pplnames.indexOf(",") + 1));
                
                var vehiclenos = $(nRow).find("td:eq(4)").html();                
                $(nRow).find("td:eq(4)").html(vehiclenos.substr(vehiclenos.indexOf(",") + 1));
                
                $(nRow).attr("id", aData['gp_id']);
                $('td', nRow).contents().wrap('<a onclick="fetchidinfo(this)" data-toggle="modal" href="#popup-basic" />');             
                
                return nRow;
            }
        });
    } else {
        $('#pendingAppointmentstab').DataTable({
            "processing": true,
            "serverSide": false,                            
            "ajax": {
                url: hostName+"/api/apmnt/sectoapproveApproved/1",
                type: "GET",
                async: false,
                dataType: "json",                   
                headers:{
                    "Content-Type":"application/json",
                    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                    "Authorization": bauth,
                }
            },                            
            "columns": [
                { "data": "gp_id" },
                { "data": "gp_visitor_name" },
                { "data": "gp_visitor_accomp_persons" } ,
                { "data": "gp_visitor_vehicle_no" },  
                { "data": "gp_tomeet_from" } 
            ],
            "bFilter" : false, 
            "columnDefs": [
                                { "orderable": false, "targets": [] }
                            ],
                            "order": [[ 0, 'desc' ]],               
            "iDisplayLength": 25,
            "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, 'All']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {                
                var pplnames = $(nRow).find("td:eq(3)").html();                                
                $(nRow).find("td:eq(3)").html(pplnames.substr(pplnames.indexOf(",") + 1));
                
                var vehiclenos = $(nRow).find("td:eq(4)").html();                
                $(nRow).find("td:eq(4)").html(vehiclenos.substr(vehiclenos.indexOf(",") + 1));
                
                $(nRow).attr("id", aData['gp_id']);
                $('td', nRow).contents().wrap('<a onclick="fetchidinfo(this)" data-toggle="modal" href="#popup-basic" />');
                
                return nRow;
            }                                   
        });
    }
}

function drawIgnoredAppointmentsTableMultiple() {
    var userobject = sessionStorage.getItem('user');
    var userfobj = JSON.parse(userobject);
    
    if ($.fn.dataTable.isDataTable('#ignoredAppointmentstab')) {
        $('#ignoredAppointmentstab').DataTable({
            "destroy": true,
            "processing": true,
            "serverSide": false,                            
            "ajax": {
                url: hostName+"/api/apmnt/sectoapproveIgnored/2",
                type: "GET",
                async: false,
                dataType: "json",                   
                headers:{
                    "Content-Type":"application/json",
                    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                    "Authorization": bauth,
                }
            },                          
            "columns": [
                { "data": "gp_id" },
                { "data": "gp_visitor_name" },
                { "data": "gp_visitor_accomp_persons" } ,
                { "data": "gp_visitor_vehicle_no" },  
                { "data": "gp_tomeet_from" }                     
            ],
            "bFilter" : false,     
            "columnDefs": [
                                { "orderable": false, "targets": [] }
                            ],
                            "order": [[ 0, 'desc' ]],           
            "iDisplayLength": 25,
            "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, 'All']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                var pplnames = $(nRow).find("td:eq(3)").html();                                
                $(nRow).find("td:eq(3)").html(pplnames.substr(pplnames.indexOf(",") + 1));
                
                var vehiclenos = $(nRow).find("td:eq(4)").html();                
                $(nRow).find("td:eq(4)").html(vehiclenos.substr(vehiclenos.indexOf(",") + 1));
                
                $(nRow).attr("id", aData['gp_id']);
                $('td', nRow).contents().wrap('<a onclick="fetchidinfo(this)" data-toggle="modal" href="#popup-basic" />');
                
                return nRow;
            }
        });
    } else {
        $('#ignoredAppointmentstab').DataTable({
            "processing": true,
            "serverSide": false,                            
            "ajax": {
                url: hostName+"/api/apmnt/sectoapproveIgnored/2",
                type: "GET",
                async: false,
                dataType: "json",                   
                headers:{
                    "Content-Type":"application/json",
                    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                    "Authorization": bauth,
                }
            },                            
            "columns": [
                { "data": "gp_id" },
                { "data": "gp_visitor_name" },
                { "data": "gp_visitor_accomp_persons" } ,
                { "data": "gp_visitor_vehicle_no" },  
                { "data": "gp_tomeet_from" } 
            ],
            "bFilter" : false, 
            "columnDefs": [
                                { "orderable": false, "targets": [] }
                            ],
                            "order": [[ 0, 'desc' ]],               
            "iDisplayLength": 25,
            "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, 'All']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                var pplnames = $(nRow).find("td:eq(3)").html();                                
                $(nRow).find("td:eq(3)").html(pplnames.substr(pplnames.indexOf(",") + 1));
                
                var vehiclenos = $(nRow).find("td:eq(4)").html();                
                $(nRow).find("td:eq(4)").html(vehiclenos.substr(vehiclenos.indexOf(",") + 1));
                
                $(nRow).attr("id", aData['gp_id']);
                $('td', nRow).contents().wrap('<a onclick="fetchidinfo(this)" data-toggle="modal" href="#popup-basic" />');
                
                return nRow;
            }                                   
        });
    }
}