var emailre =  /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

function getOfficers(checked, prefix){
	gpassregister.getOfficers(checked, prefix);
}

function checkSession() {
	var user = sessionStorage.getItem('user');
	
	if(!user) {
		window.location.href = hostName+"/admin/index.html";
	}
}

$(".right-nav ul li").on("click tap", function(){
	$(".right-nav ul li.active").removeClass('active');
	$(this).addClass('active');	
});

checkSession();

function pagination(ele){
	if($(ele).hasClass('active'))
		return false;
		
	var c = parseInt($(ele).attr('data-id'));
	var f = $("#paging li.startpage").attr('data-id');
	var l = $("#paging li.endpage").attr('data-id');
	
	$("#paging li.pagenum").removeClass('active');
	$(ele).addClass('active');
	
	if(c == f) {
		$("#paging li.startpage").addClass('active');
		$("#paging li.prevpage").addClass('active');
		$("#paging li.prevpage").attr('data-id', 1);
	} else {		
		$("#paging li.startpage").removeClass('active');
		$("#paging li.prevpage").removeClass('active');
		$("#paging li.prevpage").attr('data-id', c-1);
	}
	
	if(c == l) {
		$("#paging li.endpage").addClass('active');
		$("#paging li.nextpage").addClass('active');
		$("#paging li.nextpage").attr('data-id', l);
	} else {		
		$("#paging li.endpage").removeClass('active');
		$("#paging li.nextpage").removeClass('active');
		$("#paging li.nextpage").attr('data-id', c+1);
	}
	
	$("#list_inner_table table tbody#listtablereg tr").addClass('hiddenrows');
	$("#list_inner_table table tbody#listtablereg tr.userrowclass_"+c).removeClass('hiddenrows');
}

function pagination2(pn){
	var c = parseInt(pn);
	var f = $("#paging li.startpage").attr('data-id');
	var l = $("#paging li.endpage").attr('data-id');
	
	$("#paging li.pagenum").removeClass('active');
	$("#paging li.pagenum[data-id="+pn+"]").addClass('active');
	
	if(c == f) {
		$("#paging li.startpage").addClass('active');
		$("#paging li.prevpage").addClass('active');
		$("#paging li.prevpage").attr('data-id', 1);
	} else {		
		$("#paging li.startpage").removeClass('active');
		$("#paging li.prevpage").removeClass('active');
		$("#paging li.prevpage").attr('data-id', c-1);
	}
	
	if(c == l) {
		$("#paging li.endpage").addClass('active');
		$("#paging li.nextpage").addClass('active');
		$("#paging li.nextpage").attr('data-id', l);
	} else {	
		$("#paging li.endpage").removeClass('active');
		$("#paging li.nextpage").removeClass('active');	
		$("#paging li.nextpage").attr('data-id', c+1);
	}
	
	$("#list_inner_table table tbody#listtablereg tr").addClass('hiddenrows');
	$("#list_inner_table table tbody#listtablereg tr.userrowclass_"+c).removeClass('hiddenrows');
}

$(document).ready(function(){
		
	$("#showlist").on('click tap', function(){
		
		$("#newreg_section").hide();
		$("#myaccount_section").hide();	
		$("#reports_section").hide();
		
		/*
		$("#list_reg_section #listtablereg").html('');
		
		var list = gpassregister.getAllUsers();
		var text;
		
		if(isNaN(list)){
			var trclass = 1;
			var paging = '';
			var hidenclass = '';
			for(var i=0; i<list.length; i++) {						
				if(i%15 == 0 && i > 1) {					
					trclass++;	
					hidenclass = ' hiddenrows';
					
					if(trclass <= 10)
						paging += "<li onclick='pagination(this)' data-id='"+trclass+"' class='pagenum'><span>"+trclass+"</span></li>";
				}			
					 
				text = "<tr class='userrowclass_"+trclass+hidenclass+"'><td><a class='whitelink' href='#' onclick='editUser("+list[i].gp_usr_id+")'>"+(i+1)+"</a></td><td><a class='whitelink' href='#' onclick='editUser("+list[i].gp_usr_id+")'>"+list[i].gp_usr_emp_name+"</a></td><td><a class='whitelink' href='#' onclick='editUser("+list[i].gp_usr_id+")'>"+list[i].gp_usr_emp_emailid+"</a></td><td><a class='whitelink' href='#' onclick='editUser("+list[i].gp_usr_id+")'>"+list[i].gp_usr_emp_mobileno+"</a></td><td><a class='whitelink' href='#' onclick='editUser("+list[i].gp_usr_id+")'>"+list[i].gp_usr_emp_designation+"</a></td><td><a class='whitelink' href='#' onclick='editUser("+list[i].gp_usr_id+")'>"+list[i].gp_usr_emp_type+"</a></td></tr>";
				$("#list_reg_section #listtablereg").append(text);				
			}	
			if(trclass > 1)
				paging = "<ul id='paging'><li onclick='pagination(this)' data-id='1' class='startpage pagenum active'><span>&lt;&lt;start</span></li><li onclick='pagination(this)' data-id='1' class='prevpage pagenum active'><span>&lt;prev</span></li><li onclick='pagination(this)' data-id='1' class='pagenum active'><span>1</span></li>"+paging+"<li onclick='pagination(this)' data-id='2' class='nextpage pagenum'><span>next&gt;</span></li><li onclick='pagination(this)' data-id='"+trclass+"' class='endpage pagenum'><span>end&gt;&gt;</span></li></ul>";
			else
				paging = "<ul id='paging'><li data-id='1' class='pagenum active'><span>1</span></li></ul>";	
			
			var pn = $("#paging li.active").attr('data-id');
			
			$("#list_reg_section #list_inner_table #paging").remove();
			$("#list_reg_section #list_inner_table").append(paging);	
			
			if(typeof(pn) != 'undefined')	
				pagination2(pn);
		}
		else{
			text = "<tr><td colspan='6'>No records.</td></tr>";
			$("#list_reg_section #listtablereg").append(text);	
		}
		
		
		window.setTimeout(function(){if(!$("#list_reg_section").is(':hidden'))$("#showlist").trigger('tap');}, 60000);	*/
		
		$("#list_reg_section").show();
		drawusertable();    
    	window.setTimeout(function(){if(!$("#list_reg_section").is(':hidden') && $("#edituser.modal").is(':hidden'))$("#showlist").trigger('tap');}, 60000);			
	});
	
	function drawusertable() {
        if ($.fn.dataTable.isDataTable('#list_inner_table_table')) {        	
            $('#list_inner_table_table').DataTable({
                "destroy": true,
                "processing": true,
		        "serverSide": false,                            
		        "ajax": {
		            "url": hostName+"/api/allusers",
		            "type": "GET",
		            "async": false,
		            "dataType": "json",
		            headers:{
		                "Content-Type":"application/json",
		                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
		                "Authorization": bauth,
		            },
		        },                            
		        "columns": [
		        	{ "data": "gp_usr_id" },
		            { "data": "gp_usr_emp_name" },
		            { "data": "gp_usr_emp_emailid" },
		            { "data": "gp_usr_emp_mobileno" },
		            { "data": "gp_usr_emp_designation" },
		            { "data": "gp_usr_emp_type" } 
		        ],
		        "bFilter" : false,
		        "columnDefs": [
				    { "orderable": false, "targets": [0,4,5] }
			  	],
			  	"order": [[ 1, 'asc' ]],			  	
		        "iDisplayLength": 25,
		        "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, 'All']],
		        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
		            $(nRow).attr("id", aData['gp_usr_id']); 
		            $(nRow).find("td:first-child").html(iDisplayIndex+1); //$(nRow).prepend("<td>"+(iDisplayIndex+1)+"</td>");                 
		            $('td', nRow).contents().wrap('<a class="whitelink" href="#" onclick="editUser('+aData['gp_usr_id']+')" />');
		            return nRow;
		        }
            });
        } else {
            $('#list_inner_table_table').DataTable({
		        "processing": true,
		        "serverSide": false,                            
		        "ajax": {
		            "url": hostName+"/api/allusers",
		            "type": "GET",
		            "async": false,
		            "dataType": "json",
		            headers:{
		                "Content-Type":"application/json",
		                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
		                "Authorization": bauth,
		            },
		        },                            
		        "columns": [
		        	{ "data": "gp_usr_id" },
		            { "data": "gp_usr_emp_name" },
		            { "data": "gp_usr_emp_emailid" },
		            { "data": "gp_usr_emp_mobileno" },
		            { "data": "gp_usr_emp_designation" },
		            { "data": "gp_usr_emp_type" } 
		        ],
		        "bFilter" : false,
		        "columnDefs": [
				    { "orderable": false, "targets": [0,4,5] }
			  	],
			  	"order": [[ 1, 'asc' ]],
		        "iDisplayLength": 25,
		        "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, 'All']],
		        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
		            $(nRow).attr("id", aData['gp_usr_id']); 
		            $(nRow).find("td:first-child").html(iDisplayIndex+1); //$(nRow).prepend("<td>"+(iDisplayIndex+1)+"</td>");                   
		            $('td', nRow).contents().wrap('<a class="whitelink" href="#" onclick="editUser('+aData['gp_usr_id']+')" />');
		            return nRow;
		        }
		    });
        }
    }
               
               
	$("#showlist").trigger('tap');
	
	$("#shownew").on('click tap', function(){			
		$('#modalmsg').empty();
		$('#gp_usr_emp_name').val('');
		$('#gp_usr_emp_emailid').val('');
	   	$('#gp_usr_emp_mobileno').val('');
	   	$('#gp_usr_emp_passwd').val('');
	   	$('#gp_usr_emp_deptmnt').val('');
	   	$('#gp_usr_emp_designation').val('');
	   	$('#gp_usr_emp_addtional_info').val('');
	   	$("#gp_usr_emp_proxy").removeAttr("checked");
       	$("#officerslist").html('');
		$("#newreg_section").show();
		$("#myaccount_section").hide();
		$("#list_reg_section").hide();
		$("#reports_section").hide();
	});
	
	$("#showmyaccount").on('click tap', function(){		
		var user = JSON.parse(sessionStorage.getItem('user'));
		
		$('#myac_gp_usr_emp_name').val(user.gp_usr_emp_name);
	    $('#myac_gp_usr_emp_emailid').val(user.gp_usr_emp_emailid);
	    $('#myac_gp_usr_emp_mobileno').val(user.gp_usr_emp_mobileno);
	    $('#myac_gp_usr_emp_passwd').val('');
	    $('#myac_gp_usr_emp_deptmnt').val(user.gp_usr_emp_deptmnt);
	    $('#myac_gp_usr_emp_designation').val(user.gp_usr_emp_designation);
	    $('#myac_gp_usr_emp_addtional_info').val(user.gp_usr_emp_addtional_info);
	    		
		$("#newreg_section").hide();
		$("#myaccount_section").show();
		$("#list_reg_section").hide();
		$("#reports_section").hide();
	});
	
	$("#showreports").on('click tap', function(){		
		
	    $("#reports_section").show();		
		$("#newreg_section").hide();
		$("#myaccount_section").hide();
		$("#list_reg_section").hide();
	});
	
	$("#showlogout").on('click tap', function(){		
		$("#showlist").off('click');
		$("#showlist").off('tap');
		sessionStorage.removeItem('user');		
		checkSession();		
	});
	
	$('#edituser').on('hidden.bs.modal', function() {
		$("#showlist").trigger('tap');
	});
});

function editUser(id) {	
	checkSession();
	var user = gpassregister.getuserbyid(id);	
	
	$('#edit_gp_usr_emp_name').val(user.gp_usr_emp_name);
    $('#edit_gp_usr_emp_emailid').val(user.gp_usr_emp_emailid);
    $('#edit_gp_usr_emp_mobileno').val(user.gp_usr_emp_mobileno);
    $('#edit_gp_usr_emp_passwd').val('');
    $('#edit_gp_usr_emp_deptmnt').val(user.gp_usr_emp_deptmnt);
    $('#edit_gp_usr_emp_designation').val(user.gp_usr_emp_designation);
    $('#edit_gp_usr_emp_addtional_info').val(user.gp_usr_emp_addtional_info);
    $('#edit_gp_usr_id').val(user.gp_usr_id);
    
    if(user.gp_usr_emp_proxy == 1) {
    	$("#edit_gp_usr_emp_proxy").attr("checked", true);
    	$("#edit_officerslist").html('');
    	gpassregister.getOfficers(true, "edit_");
    	$("#edit_gp_usr_emp_proxy_for").find("option[value="+user.gp_usr_emp_proxy_for+"]").attr("selected", "selected");
    } else {
    	$("#edit_gp_usr_emp_proxy").removeAttr("checked");
    	 $("#edit_officerslist").html('');
    }   
    
    $('#edituser').modal('show');
}

function updateUser() {
	checkSession();
	var id = $('#edit_gp_usr_id').val();	
	
    var emp_proxy, emp_proxy_for;
      	
    if(document.getElementById("edit_gp_usr_emp_proxy").checked) {
    	emp_proxy = 1;
    	emp_proxy_for = document.getElementById("edit_gp_usr_emp_proxy_for").value;
    }
    else {
    	emp_proxy = 0;
    	emp_proxy_for = 0;
    } 
	
	params = {              
                  "gp_usr_emp_name": document.getElementById("edit_gp_usr_emp_name").value,
                  "gp_usr_emp_emailid": document.getElementById("edit_gp_usr_emp_emailid").value,                  
                  "gp_usr_emp_mobileno": document.getElementById("edit_gp_usr_emp_mobileno").value,
                  "gp_usr_emp_deptmnt": document.getElementById("edit_gp_usr_emp_deptmnt").value,
                  "gp_usr_emp_designation": document.getElementById("edit_gp_usr_emp_designation").value,                  
                  "gp_usr_emp_addtional_info": document.getElementById("edit_gp_usr_emp_addtional_info").value,                  
                  "gp_usr_emp_proxy": emp_proxy,
  				  "gp_usr_emp_proxy_for": emp_proxy_for,      
  				  "gp_usr_emp_passwd": CryptoJS.MD5(document.getElementById("edit_gp_usr_emp_passwd").value)            
             };
                     
    if(document.getElementById("edit_gp_usr_emp_passwd").value.trim() != '') {
		var passwd = CryptoJS.MD5(document.getElementById("edit_gp_usr_emp_passwd").value);
	    passwd = passwd.toString();	 
	    params.gp_usr_emp_passwd = passwd;
	}           

 	data = JSON.stringify(params);        
	resp = gpassregister.updateUser(data, id);
	
	$('#edit_modalmsg').text('');
    $('#edit_modalmsg').text('User has been updated successfully');
}

function registersubmit(){
	checkSession();
   	var usrmobileno = document.getElementById("gp_usr_emp_mobileno").value; 
     $(".errline").removeClass('errline');
    if(document.getElementById("gp_usr_emp_name").value.length==0)
    {
        $('#errmsg').empty();
        $('#errmsg').text('Please enter name');
        
        $("#gp_usr_emp_name").addClass('errline');
    }else if(document.getElementById("gp_usr_emp_emailid").value=="" || !emailre.test(document.getElementById("gp_usr_emp_emailid").value))
    {
        $('#errmsg').empty();
        $('#errmsg').text('Please enter valid Email');
        $("#gp_usr_emp_emailid").addClass('errline'); 
    }else if(document.getElementById("gp_usr_emp_mobileno").value.length==0)
    {
       $('#errmsg').empty();
        $('#errmsg').text('Please enter Phone Number');
        
$("#gp_usr_emp_mobileno").addClass('errline');

    }else if(document.getElementById("gp_usr_emp_mobileno").value.length != 10 )
   
    {
        $('#errmsg').empty();
        $('#errmsg').text('Please enter Valid 10 digit Mobile Number');
        
        $("#gp_usr_emp_mobileno").addClass('errline');
        
    }else if(document.getElementById("gp_usr_emp_passwd").value.length == 0 )
    {
      $('#errmsg').empty();
        $('#errmsg').text('Please enter Password');
        
         $("#gp_usr_emp_passwd").addClass('errline');
        
    }else if(document.getElementById("gp_usr_emp_passwd").value.length < 6 )
    {
      $('#errmsg').empty();
      $('#errmsg').text('Please enter min 6 characters of passwd');
        
      $("#gp_usr_emp_passwd").addClass('errline');
        
    }else if(document.getElementById("gp_usr_emp_type").value == 0 )
    {
      $('#errmsg').empty();
        $('#errmsg').text('Please select Employee type');
        
        $("#gp_usr_emp_type").addClass('errline');
        
    }else 
    {
      
      var emailid = document.getElementById("gp_usr_emp_emailid").value ;
      var userdata = gpassregister.getuserbyemail(emailid);     
      
        if(typeof(userdata) != "undefined" && userdata.gp_usr_emp_emailid){
          var msg = "Email already exists "+userdata.gp_usr_emp_emailid;
           $('#modalmsg').empty();
          $('#modalmsg').text(msg);
           $('#messagemodal').modal('show');
     
       
             }else{
            var mobileno = document.getElementById("gp_usr_emp_mobileno").value ;
            var userdata = gpassregister.getuserbymobile(mobileno);
         if(typeof(userdata) != "undefined" && userdata.gp_usr_emp_emailid){
          var msg = "Mobile No already exists "+userdata.gp_usr_emp_mobileno;
           $('#modalmsg').empty();
          $('#modalmsg').text(msg);
           $('#messagemodal').modal('show');
          
           }else{
          
          var params;
     var registerdata;
     var emptype;
     var empdept="";
     var empdeg;
     var empaddinfo;
     var passwd = CryptoJS.MD5(document.getElementById("gp_usr_emp_passwd").value);
      passwd = passwd.toString();
          
          if (document.getElementById("gp_usr_emp_type").value == 1){
            emptype ="officer";
          }else if(document.getElementById("gp_usr_emp_type").value == 2){
            emptype ="officer2";
          }else if(document.getElementById("gp_usr_emp_type").value == 3){
            emptype ="Security";
          }else if(document.getElementById("gp_usr_emp_type").value == 4){
            emptype ="gkeeper";
          }else if(document.getElementById("gp_usr_emp_type").value == 5){
            emptype ="vofficer";
          };
      
      	var emp_proxy, emp_proxy_for;
      	
        if(document.getElementById("gp_usr_emp_proxy").checked) {
        	emp_proxy = 1;
        	emp_proxy_for = document.getElementById("gp_usr_emp_proxy_for").value;
        }
        else {
        	emp_proxy = 0;
        	emp_proxy_for = 0;
        }
        
         
   params = {                 
                  "gp_usr_emp_role": document.getElementById("gp_usr_emp_type").value,
                  "gp_usr_emp_name": document.getElementById("gp_usr_emp_name").value,
                  "gp_usr_emp_emailid": document.getElementById("gp_usr_emp_emailid").value,
                  "gp_usr_emp_passwd": passwd,
                  "gp_usr_emp_mobileno": document.getElementById("gp_usr_emp_mobileno").value,
                  "gp_usr_emp_deptmnt": document.getElementById("gp_usr_emp_deptmnt").value,
                  "gp_usr_emp_designation": document.getElementById("gp_usr_emp_designation").value,
                  "gp_usr_emp_photo_url": "null",
                  "gp_usr_emp_addtional_info": document.getElementById("gp_usr_emp_addtional_info").value,
                  "gp_usr_emp_type": emptype,
                  "gp_usr_emp_proxy": emp_proxy,
  				  "gp_usr_emp_proxy_for": emp_proxy_for,                  
                };

        registerdata = JSON.stringify(params);
  
        var resp = gpassregister.registeruser(registerdata);   
        
        if(typeof(resp.gp_usr_id) != 'undefined') {
        	var text = 'Your registration at APS Gate Pass has been confirmed. Your gate pass id is: '+resp.gp_usr_id;
        	gpassnotification.sms("91"+resp.gp_usr_emp_mobileno, text); 
        	
        	var body = 'Dear '+resp.gp_usr_emp_name+', <br/>Your registration has been successfully completed. Your gate pass id is: '+resp.gp_usr_id+'<br/> Thanks<br/>APS Secretariat';
        	var subject = 'Your registration at APS Gate Pass has been confirmed.';
        	gpassnotification.email(resp.gp_usr_emp_emailid, subject, body); 
        }
        
           $('#modalmsg').empty();
           $('#gp_usr_emp_name').val('');
           $('#gp_usr_emp_emailid').val('');
           $('#gp_usr_emp_mobileno').val('');
           $('#gp_usr_emp_passwd').val('');
           $('#gp_usr_emp_deptmnt').val('');
           $('#gp_usr_emp_designation').val('');
           $('#gp_usr_emp_addtional_info').val('');
           $('#gp_usr_emp_type').val('');
           $("#gp_usr_emp_proxy").removeAttr("checked");
           $("#officerslist").html('');
           $('#errmsg').empty();
           $('#modalmsg').text('User has been registered successfully');
           $('#messagemodal').modal('show');
          
        }
          
        }
   
  }
  
};

//Reports related scripts
function fetchidinfo (sender) {
    $("#errmsg").text('');
    var t = sender.parentNode.parentNode;
    var pid = t.getAttribute('id'); 

    $.ajax({
        type: "GET",
        url: hostName+"/api/apmnt/"+pid,
        async: false,
        dataType: "json",
        headers:{
            "Content-Type":"application/json",
             "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
            "Authorization": bauth,
        },
        success: function (data){
            if(data != null){
                var succdata = data;
                var apmntdata = succdata.data;
                var entrytype ; 
                if(apmntdata.gp_visitor_apmnt_options==1){
                    entrytype ="Single";
                }else if(apmntdata.gp_visitor_apmnt_options==2){
                    entrytype ="Multiple";
                }
                var noofvehicle = apmntdata.gp_visitor_vehicle_no;
                var vehiclenos = apmntdata.gp_visitor_vehicle_no;
                vehiclenos = vehiclenos.substr(vehiclenos.indexOf(",") + 1);
                noofvehicle = noofvehicle.split(",");
                var noofppl = apmntdata.gp_visitor_accomp_persons;
                var pplnames = apmntdata.gp_visitor_accomp_persons;
                pplnames = pplnames.substr(pplnames.indexOf(",") + 1);
                noofppl = noofppl.split(",");
                var fnoppl = Number(noofppl[0]) ;
                $('#apmntdetailinfo').empty();
        
                $('#apmntdetailinfo').append('<tr><td>Visiting Id </td><td id="#apmntidval">'+apmntdata.gp_id+'</td></tr><tr><td>Entry Type</td><td>'+entrytype+'</td></tr><tr><td>Entry From Time</td><td>'+apmntdata.gp_tomeet_from+'</td></tr><tr><td>Entry To Time</td><td>'+apmntdata.gp_tomeet_to+'</td></tr><tr><td>Visiting Officer Name</td><td>'+apmntdata.gp_tomeet_name+'</td></tr><tr><td>Visitor Name</td><td>'+apmntdata.gp_visitor_name+'</td></tr><tr><td>Visitor Email id</td><td>'+apmntdata.gp_visitor_email+'</td></tr><tr><td>Visitor Mobile No</td><td>'+apmntdata.gp_visitor_mobile_no+'</td></tr><tr><td>Visitor ID Proof</td><td>'+apmntdata.gp_visitor_idproof+'</td></tr><tr><td>Visitor Address</td><td>'+apmntdata.gp_visitor_address+'</td></tr><tr><td>No of Vehicles</td><td><input type="text" id="'+apmntdata.gp_id+'novehicle"value="'+noofvehicle[0]+'"</td></tr><tr><td>Visitor Vehicle No(s)</td><td><input type="text" id="'+apmntdata.gp_id+'vehiclenos"value="'+vehiclenos+'"</td></tr><tr><td>No of Accompanying persons </td><td><input type="text" id="'+apmntdata.gp_id+'noppl"value="'+fnoppl+'"></td></tr><tr><td>Visitor Accompanying persons</td><td><input type="text" id="'+apmntdata.gp_id+'names"value="'+pplnames+'" ></td></tr></td></tr><tr><td>Purpose of Visit</td><td>'+apmntdata.gp_visitor_notes+'</td></tr><tr><td>Notes to Security Officer</td><td> <input type="text" id="'+apmntdata.gp_id+'ofcnotes"value="'+apmntdata.gp_officer_approval_notes+'"></td></tr>');
            }
        }
    });  
}

function fetchidinfo2 (sender) {
	$("#errmsg").text('');
    var t = sender.parentNode.parentNode;
    pid = t.getAttribute('id'); 

  $.ajax({
      type: "GET",
      url:hostName+"/api/apmnt/"+pid,
      async: false,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var apmntdata = succdata.data;
         var entrytype ; 
          if(apmntdata.gp_visitor_apmnt_options==1){
            entrytype ="Single";
          }else if(apmntdata.gp_visitor_apmnt_options==2){
            entrytype ="Multiple";
          }
         var noofvehicle = apmntdata.gp_visitor_vehicle_no;
         var vehiclenos = apmntdata.gp_visitor_vehicle_no;
         vehiclenos = vehiclenos.substr(vehiclenos.indexOf(",") + 1);
            noofvehicle = noofvehicle.split(",");
       var noofppl = apmntdata.gp_visitor_accomp_persons;
       var pplnames = apmntdata.gp_visitor_accomp_persons;
         pplnames = pplnames.substr(pplnames.indexOf(",") + 1);
          noofppl = noofppl.split(",");
          var fnoppl = Number(noofppl[0]) ;
        $('#reportsapmntdetailinfo').empty();
        
    $('#reportsapmntdetailinfo').append('<tr><td>Visiting Id </td><td id="#apmntidval">'+apmntdata.gp_id+'</td></tr><tr><td>Entry Type</td><td>'+entrytype+'</td></tr><tr><td>Entry From Time</td><td>'+apmntdata.gp_tomeet_from+'</td></tr><tr><td>Entry To Time</td><td>'+apmntdata.gp_tomeet_to+'</td></tr><tr><td>Visiting Officer Name</td><td>'+apmntdata.gp_tomeet_name+'</td></tr><tr><td>Visitor Name</td><td>'+apmntdata.gp_visitor_name+'</td></tr><tr><td>Visitor Email id</td><td>'+apmntdata.gp_visitor_email+'</td></tr><tr><td>Visitor Mobile No</td><td>'+apmntdata.gp_visitor_mobile_no+'</td></tr><tr><td>Visitor ID Proof</td><td>'+apmntdata.gp_visitor_idproof+'</td></tr><tr><td>Visitor Address</td><td>'+apmntdata.gp_visitor_address+'</td></tr><tr><td>No of Vehicles</td><td><input type="text" id="'+apmntdata.gp_id+'novehicle"value="'+noofvehicle[0]+'"</td></tr><tr><td>Visitor Vehicle No(s)</td><td><input type="text" id="'+apmntdata.gp_id+'vehiclenos"value="'+vehiclenos+'"</td></tr><tr><td>No of Accompanying persons </td><td><input type="text" id="'+apmntdata.gp_id+'noppl"value="'+fnoppl+'"></td></tr><tr><td>Visitor Accompanying persons</td><td><input type="text" id="'+apmntdata.gp_id+'names"value="'+pplnames+'" ></td></tr></td></tr><tr><td>Purpose of Visit</td><td>'+apmntdata.gp_visitor_notes+'</td></tr><tr><td>Notes to Security Officer</td><td> <input type="text" id="'+apmntdata.gp_id+'ofcnotes"value="'+apmntdata.gp_officer_approval_notes+'"></td></tr>');
            }}});
 
  
  
}

function declinebyofc(){
  $("#errmsg").text('');
  var params;
     var registerdata;
     var enttype = $( '#etype' ).val();
     var fromdate = $('#fromdate').val();
     var fromtime = $('#fromtime').val();
     var  ffrom = fromdate+" "+fromtime+":00" ;
     var todate = $('#todate').val();
     var totime = $('#totime').val();
     var  fto = todate+" "+totime+":00" ;  
     var ofcnotes = pid+"ofcnotes";
   params = {
                  "gp_officer_approval_flag": "2",
                  "gp_officer_approval_notes": document.getElementById(ofcnotes).value,
                  "gp_security_approval_flag": "0",
                  "gp_security_approval_notes": "secappnotes",
                  "gp_gkeep_dispatch_count": 0,
                  "gp_apmnt_last_visit": "2014-12-20 06:16:14"
                  
                };

  registerdata = JSON.stringify(params);
  var apmntidval = $('#apmntidval').text();
  
  $.ajax({
      type: "PUT",
      url:hostName+"/api/apmnt/updateofcflag/"+pid,
      async: false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var alertdata = succdata.data;
         var msg = "Your Gate Pass request ignored   "+alertdata.gp_id;
        
         
         alert(msg);
         pendingapprovals();
         $('#popup-basic').modal('hide');
                  
        }
      },
      }); 
}


function approvebyofc(){
  $("#errmsg").text('');
  var params;
     var registerdata;
     var enttype = $( '#etype' ).val();
     var fromdate = $('#fromdate').val();
     var fromtime = $('#fromtime').val();
     var  ffrom = fromdate+" "+fromtime+":00" ;
     var todate = $('#todate').val();
     var totime = $('#totime').val();
     var  fto = todate+" "+totime+":00" ;  
     var ofcnotes = pid+"ofcnotes";
     var vstr1 = pid+"novehicle";
     var vstr2 = pid+"vehiclenos";
     var fvstr = document.getElementById(vstr1).value+","+document.getElementById(vstr2).value ;
     var pstr1 = pid+"noppl";
     var pstr2 = pid+"names";
     var fpstr = document.getElementById(pstr1).value+","+document.getElementById(pstr2).value ;
         
   params = { 
                  "gp_visitor_vehicle_no": fvstr ,
                  "gp_visitor_accomp_persons": fpstr,
                  "gp_officer_approval_flag": "1",
                  "gp_officer_approval_notes":document.getElementById(ofcnotes).value,
                  "gp_security_approval_flag": "0",
                  "gp_security_approval_notes": "secappnotes",
                  "gp_gkeep_dispatch_count": 0,
                  "gp_apmnt_last_visit": "2014-12-20 06:16:14"
                  
                };

  registerdata = JSON.stringify(params);
  var apmntidval = $('#apmntidval').text();
  
  $.ajax({
      type: "PUT",
      url:hostName+"/api/apmnt/updatevisitinfo/"+pid,
      async: false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                 "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var alertdata = succdata.data;
         var msg = "Your Gate Pass request forwarded successfully and please note the following gate pass id "+alertdata.gp_id+" for your reference";
         
         
         //alert(msg);
         pendingapprovals();
         $('#popup-basic').modal('hide');
         
        }
      },
      });
}

$(document).ready(function(){	            
    $('#showreports').on('click', function(){         
        drawtable();
    });
    
    $("#search_data_btn").on("click", function() {
        if($("#search_fromdate").val() == '') {
            alert("Please enter from date");
            return false;
        } else if($("#search_todate").val() == '') {
            alert("Please enter to date");
            return false;
        }
        
        drawtable();
    });

    $('#search_todate').datetimepicker({
        timepicker:false,
        format:'d-m-Y',
        //minDate:'0',
        minTime: '09:00',
        maxTime: '19:00'
    });

    $('#search_fromdate').datetimepicker({
        timepicker:false,
        format:'d-m-Y',
        //minDate:'0',
        minTime: '09:00',
        maxTime: '19:00'
    });

    $("#export_data_btn").click(function (e) {  
        if($("#search_fromdate").val() == '') {
            alert("Please enter from date");
            return false;
        } else if($("#search_todate").val() == '') {
            alert("Please enter to date");
            return false;
        }        

        $.ajax({
            type: "POST",
            url: hostName+"/api/apmnt/searchappointmentsbydate",
            async: false,
            "dataType": "json",
            "data" : {"sdate": $("#search_fromdate").val(), 
                    "edate": $("#search_todate").val(),
                    "export": "csv"},
            headers:{
                "Content-Type":"application/json",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
            success: function (file){ 
                if(file.name != "null"){                                        
                    location.href = hostName+"/api/download.php?file="+file.name;
                }
            }
        });                            
    });

    $("#print_data_btn").click(function (e) {
        if($("#search_fromdate").val() == '') {
            alert("Please enter from date");
            return false;
        } else if($("#search_todate").val() == '') {
            alert("Please enter to date");
            return false;
        }
        
        drawtable();
        printreport();
        e.preventDefault();
    });

    function drawtable() {
        if ($.fn.dataTable.isDataTable('#reports_section_table')) {
            $('#reports_section_table').DataTable({
                "destroy": true,
                "processing": true,
                "serverSide": false,                            
                "ajax": {
                    "url": hostName+"/api/apmnt/searchappointmentsbydate",
                    "type": "POST",
                    async: false,
                    dataType: "json",
                    "data" : {"sdate": $("#search_fromdate").val(), 
                            "edate": $("#search_todate").val()},
                    headers:{
                        "Content-Type":"application/json",
                        "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                        "Authorization": bauth,
                    },
                },                            
                "columns": [
                    { "data": "gp_id" },
                    { "data": "gp_visitor_name" },
                    { "data": "gp_visitor_mobile_no" },
                    { "data": "gp_visitor_vehicle_no" },
                    { "data": "gp_visitor_address" },
                    { "data": "gp_tomeet_name" },
                    { "data": "gp_apmnt_last_visit" },
                    { "data": "gp_visitor_accomp_persons" } 
                ],
                "bFilter" : false,
                "columnDefs": [
				    { "orderable": false, "targets": [4,7] }
			  	],
			  	"order": [[ 0, 'desc' ]],
                "iDisplayLength": 25,
                "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, 'All']],
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).attr("id", aData['gp_id']);                    
                    $('td', nRow).contents().wrap('<a onclick="fetchidinfo2(this)" data-toggle="modal" href="#reports-popup-basic" />');
                    return nRow;
                }
            });
        } else {
            $('#reports_section_table').DataTable({
                "processing": true,
                "serverSide": false,                            
                "ajax": {
                    "url": hostName+"/api/apmnt/searchappointmentsbydate",
                    "type": "POST",
                    "async": false,
                    "dataType": "json",
                    "data" : {"sdate": $("#search_fromdate").val(), 
                            "edate": $("#search_todate").val()},
                    headers:{
                        "Content-Type":"application/json",
                        "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                        "Authorization": bauth,
                    },
                },                            
                "columns": [
                    { "data": "gp_id" },
                    { "data": "gp_visitor_name" },
                    { "data": "gp_visitor_mobile_no" },
                    { "data": "gp_visitor_vehicle_no" },
                    { "data": "gp_visitor_address" },
                    { "data": "gp_tomeet_name" },
                    { "data": "gp_apmnt_last_visit" },
                    { "data": "gp_visitor_accomp_persons" } 
                ],
                "bFilter" : false,
                "columnDefs": [
				    { "orderable": false, "targets": [4,7] }
			  	],
			  	"order": [[ 0, 'desc' ]],
                "iDisplayLength": 25,
                "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, 'All']],
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).attr("id", aData['gp_id']);                    
                    $('td', nRow).contents().wrap('<a onclick="fetchidinfo2(this)" data-toggle="modal" href="#reports-popup-basic" />');
                    return nRow;
                }
            });
        }
    }

    function printreport()
    { 
        var disp_setting="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
        disp_setting+="scrollbars=yes,width=1000, height=700, left=100, top=25"; 
        
        //var $rep_data = $rep_datatable.fnGetData();

        var body, head, content, title;                            

        body = '<table width="100%" cellpadding="0" cellspacing="0">'+
                $("table#reports_section_table > thead").html()+
                $("table#reports_section_table > tbody").html()+
                '</table>';                                                         

        title = "Appointment reports from: "+
                $("#search_fromdate").val()+
                " to: "+$("#search_todate").val();

        var head = "<title>" +                                         
                    title + 
                    "</title>" +
                    "<link href='../css/header.css' rel='stylesheet' />" +
                    "<style type='text/css'>" +
                    "*{margin: 0;padding: 0;} \n\
                    b{font-family: Arial,Helvetica,sans-serif; font-size: 12px;} \n\
                    body{color: #181818; font-family: 'Exo',Helvetica,Arial,sans-serif; font-size: 12px; padding:5px;} \n\
                    th{font-size: 11px;color: #000;font-weight: bold;background-color: #ccc;text-align:left;} \n\
                    td.altRow{background: #f7f7f7 !important; font-size: 11px;padding-left: 3px;} \n\
                    table{border:none !important;} img{display:none;} \n\
                    td{font-size: 14px;padding-left: 10px;padding-right: 5px;}\n\
                    tr td{border: 1px solid #000;padding-top: 4px;}\n\
                    tr th{border: 1px solid #000;}\n\
                    td a{color: #000;text-decoration: none;}" +
                    "</style>";

        content = "<html><head>" + 
                    head +
                    "</head>" +
                    "<body>" +
                    body +
                    "</body></html>";
            
        var docprint=window.open("","",disp_setting); 

        docprint.document.open(); 
        docprint.document.write(content); 
        docprint.document.close(); 
        docprint.focus(); 
        docprint.print(); 
        docprint.close();
    }
});