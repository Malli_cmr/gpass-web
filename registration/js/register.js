var bauth ="Basic YXBpbG9naW46ZDN2M2xvcDNyMTIz";
var hostName = window.location.hostname;
var emailre =  /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


function registersubmit(){
   var usrmobileno = document.getElementById("gp_usr_emp_mobileno").value; 
     
    if(document.getElementById("gp_usr_emp_name").value.length==0)
    {
        $('#errmsg').empty();
        $('#errmsg').text('Please enter name');
        
        
    }else if(document.getElementById("gp_usr_emp_emailid").value=="" || !emailre.test(document.getElementById("gp_usr_emp_emailid").value))
    {
        $('#errmsg').empty();
        $('#errmsg').text('Please enter valid Email');
         
    }else if(document.getElementById("gp_usr_emp_mobileno").value.length==0)
    {
       $('#errmsg').empty();
        $('#errmsg').text('Please enter Phone Number');
        


    }else if(document.getElementById("gp_usr_emp_mobileno").value.length != 10 )
   
    {
        $('#errmsg').empty();
        $('#errmsg').text('Please enter Valid 10 digit Mobile Number');
        
        
        
    }else if(document.getElementById("gp_usr_emp_passwd").value.length == 0 )
    {
      $('#errmsg').empty();
        $('#errmsg').text('Please enter Password');
        
        
        
    }else if(document.getElementById("gp_usr_emp_passwd").value.length < 6 )
    {
      $('#errmsg').empty();
      $('#errmsg').text('Please enter min 6 characters of passwd');
        
        
        
    }else if(document.getElementById("gp_usr_emp_type").value == 0 )
    {
      $('#errmsg').empty();
        $('#errmsg').text('Please select Employee type');
        
        
        
    }else 
    {
      
     $.ajax({
      type: "GET",
      url:"/api/userinfobyemail/"+document.getElementById("gp_usr_emp_emailid").value,
      async: false,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var userdata = succdata.data;
         if(userdata.gp_usr_emp_emailid){
          var msg = "Email already exists "+userdata.gp_usr_emp_emailid;
           $('#modalmsg').empty();
          $('#modalmsg').text(msg);
           $('#messagemodal').modal('show');
          
          
        }else{
          
             $.ajax({
      type: "GET",
      url:"/api/userinfobymbl/"+document.getElementById("gp_usr_emp_mobileno").value,
      async: false,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var userdata = succdata.data;
         if(userdata.gp_usr_emp_emailid){
          var msg = "Mobile No already exists "+userdata.gp_usr_emp_mobileno;
           $('#modalmsg').empty();
          $('#modalmsg').text(msg);
           $('#messagemodal').modal('show');
          
        }else{
          
          var params;
     var registerdata;
     var emptype;
     var empdept="";
     var empdeg;
     var empaddinfo;
     var passwd = CryptoJS.MD5(document.getElementById("gp_usr_emp_passwd").value);
      passwd = passwd.toString();
          
          if (document.getElementById("gp_usr_emp_type").value == 1){
            emptype ="officer";
          }else if(document.getElementById("gp_usr_emp_type").value == 2){
            emptype ="Security";
          }else if(document.getElementById("gp_usr_emp_type").value == 3){
            emptype ="gkeeper";
          };
      
        
        
         
   params = {
                 
                  "gp_usr_emp_role": document.getElementById("gp_usr_emp_type").value,
                  "gp_usr_emp_name": document.getElementById("gp_usr_emp_name").value,
                  "gp_usr_emp_emailid": document.getElementById("gp_usr_emp_emailid").value,
                  "gp_usr_emp_passwd": passwd,
                  "gp_usr_emp_mobileno": document.getElementById("gp_usr_emp_mobileno").value,
                  "gp_usr_emp_deptmnt": document.getElementById("gp_usr_emp_deptmnt").value,
                  "gp_usr_emp_designation": document.getElementById("gp_usr_emp_designation").value,
                  "gp_usr_emp_photo_url": "null",
                  "gp_usr_emp_addtional_info": document.getElementById("gp_usr_emp_addtional_info").value,
                  "gp_usr_emp_type": emptype 
                  
                };

  registerdata = JSON.stringify(params);
  
  $.ajax({
      type: "POST",
      url: "http://"+hostName+"/api/register",
      async: false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var errordata = succdata.error;
         
         if(errordata){
              
              alert('error');
           
         }
         
         var alertdata = succdata.data;
         
          $('#modalmsg').empty();
           $('#gp_usr_emp_name').val('');
           $('#gp_usr_emp_emailid').val('');
            $('#gp_usr_emp_mobileno').val('');
             $('#gp_usr_emp_passwd').val('');
              $('#gp_usr_emp_deptmnt').val('');
               $('#gp_usr_emp_designation').val('');
                $('#gp_usr_emp_addtional_info').val('');
            
            var params;
     var registerdata;
    var fmobileno = "91"+usrmobileno; 
    var ftext = "Your account has been registered at "+hostName+ "with password "+document.getElementById("gp_usr_emp_passwd").value; 
   params = {
                 
  "mobileno": fmobileno,
  "text": ftext

                  
                };

  registerdata = JSON.stringify(params);
  
  $.ajax({
      type: "POST",
      url:"/api/apmnt/smsnotification",
      async: false,
      data : registerdata,
      dataType: "json",
      headers:{
                "Content-Type":"application/json",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": bauth,
            },
      success: function (data){
        if(data != null){
         var succdata = data;
         var alertdata = succdata.status;
      
          $('#modalmsg').text('User has been registered successfully');
          $('#messagemodal').modal('show');
          

         
                  
        }
      },
      });    
            
      
   
                  
        }
      },
    
  
  });
          
        }
            }}});
          
          
        }
            }}});
      
     
   
  }
  
};